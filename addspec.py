#!/usr/bin/python
""" 
This script adds up two spectra and saves to an output file. Be sure that you can use the same ARF/RMF for both spectra.
"""

import pyfits as pf
import sys
from yan import *

try:
	firstfile=sys.argv[1]
	secondfile=sys.argv[2]
	outfile=sys.argv[3]
except:
	print "\nYou did not specify three command line arguments, try again" + __doc__
	

one=pf.open(firstfile)
two=pf.open(secondfile)

one[1].header['EXPOSURE']=one[1].header['EXPOSURE']+two[1].header['EXPOSURE']

one[1].data.field('COUNTS')[:]=one[1].data.field('COUNTS') + two[1].data.field('COUNTS')

fw(one,outfile)
