#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
from numpy import array, sqrt
import os
import sys
import numpy as np


def gticonv(inp,times,gtif):
	inp=array(inp)
	times=array(times)
	gf=pf.open(gtif)
	starts=gf[1].data.field("START")
	stops=gf[1].data.field("STOP")
	gf.close()
	selected=array([],dtype=int)
	for ii in range(len(starts)):
		sel=np.where(times>starts[ii])[0]
		ected=np.where(times<stops[ii])[0]
		selected=np.union1d(selected,np.intersect1d(sel,ected))
		#selected.append(np.intersect1d(sel,ected))
	return inp[selected]
		
def gtitot(inp,times,gtif):
	inp=array(inp)
	times=array(times)
	gf=pf.open(gtif)
	starts=gf[1].data.field("START")
	stops=gf[1].data.field("STOP")
	gf.close()
	
	selected=array([],dtype=int)
	
	start=starts[0]
	stop=stops[-1]
	sel=np.where(times>start)[0]
	ected=np.where(times<stop)[0]
	selected=np.union1d(selected,np.intersect1d(sel,ected))
	
	return inp[selected]	
	
	
#try:
	#SAS_ODF=os.environ["SAS_ODF"]
#except:
	#SAS_ODF=os.path.abspath("./odf")
	#sys.stderr.write("No SAS_ODF environment variable found. Assuming "+SAS_ODF+"\n")
try:
	SAS_RADMON=os.environ["SAS_RADMON"]
except:
	SAS_RADMON=os.path.abspath("./radmon")
	sys.stderr.write("No SAS_RADMON environment variable found. Assuming "+SAS_RADMON+"\n")


workdir=os.getcwd()

if "rel" in sys.argv:
	rel=1
	relstring="rel_"
else:
	rel=0
	relstring=""

if "pause" in sys.argv:
	pause=True
else:
	pause=False
	
gtif=[gt for gt in sys.argv if "gti=" in gt]

if len(gtif) is 0:
	gti=False
else:
	gti=True
	gtif=gtif[0]
	gtif=gtif.split("=")
	gtif=gtif[1]
	


os.chdir(SAS_RADMON)
fil=[f for f in os.listdir(".") if "ECE" in f]

if len(fil) is 0:
	sys.stderr.write("Can't find calibrated radmonfile. Check input")
	sys.exit(2)

means=list()
errm=0.
highs=list()
errh=0.
lows=list()
errl=0.

toths=list()
terrh=0.
totls=list()
terrl=0.

ooid=(os.getcwd()).split("/")[-2]
oid=ooid+"-"+(os.getcwd()).split("/")[-3][0:3]
	
for fl in fil:
	
	fh=pf.open(fl)
	#datT=fh[1].data.field('FTCOARSE')
	datT=fh[1].data.field('TIME')
	#datT=datT/3600.
	#datT=datT/24.
	datHi=fh[1].data.field('NLE0')
	datLo=fh[1].data.field('NHEC')
	fh.close()
	if gti:
		datHi=gticonv(datHi,datT,gtif)
		datLo=gticonv(datLo,datT,gtif)
		datT=gticonv(datT,datT,gtif)
		
		totHi=gtitot(datHi,datT,gtif)
		totLo=gtitot(datLo,datT,gtif)
	else:
		sys.stderr.write("No GTI supplied. The probability that the result doesn't make sense is pretty large since the radmon data is based on a full orbit i.s.o. the observation used")
		totHi=datHi
		totLo=datLo
	
	totls.append(totHi.mean())
	terrl+=len(totLo)
	toths.append(totLo.mean())
	terrh+=len(totHi)
			
	try:
		datT=datT-datT[0]
	except IndexError:
		continue

	
	#datHi=datHi/(sum(datHi)/len(datHi)) # To mean on average val
	#datLo=datLo/(sum(datLo)/len(datLo)) # To mean on average val
		
	datRl=datHi/datLo 
	if 0. in datLo:
		datRl[0.==datLo]=0.

	if pause:
		pl.ion()
	if rel==0:
		pl.plot(datT,datLo,"-",label="Low E")
		pl.plot(datT,datHi,"-",label="High E")
	else:
		pl.plot(datT,datRl,"-",label="Rel E")
	#pl.plot(datX,[0]*len(datX),"r-")
	means.append(datRl.mean())
	lows.append(datLo.mean())
	highs.append(datHi.mean())
	errm+=len(datRl)
	errl+=len(datLo)
	errh+=len(datHi)

	
errm=1./sqrt(errm)
errl=1./sqrt(errl)
errh=1./sqrt(errh)
	
terrl=1./sqrt(terrl)
terrh=1./sqrt(terrh)	
	
pl.suptitle("Radiation monitor data")
pl.title("obs. id: "+oid)
pl.xlabel("Time since start")
if rel==0:
	pl.ylabel("Count rate")
else:
	pl.ylabel("Relative count rate")
pl.legend(loc=0)
#ax = pl.axes()
#ax.set_xlim(max(0,ot[0]-(nsig+2)*sqrt(ot[0])),ot[0]+(nsig+2)*sqrt(ot[0]))
if pause:
	pl.draw()
	raw_input()

means=array(means)
lows=array(lows)
highs=array(highs)

totls=array(totls)
toths=array(toths)

os.chdir(workdir)

pl.savefig("radmon_"+relstring+oid+".eps")
pl.savefig("radmon_"+relstring+oid+".pdf")


print ooid, means.mean(), lows.mean(), highs.mean()
print "err",ooid, errm*means.mean(), errl*lows.mean(), errh*highs.mean()
print ooid, totls.mean(), toths.mean()
print "err",ooid, terrl*totls.mean(), terrh*toths.mean()
