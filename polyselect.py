#!/usr/bin/python

from matplotlib import path
import numpy as np
import pyfits as pf
import sys

evli = sys.argv[1]
poly = sys.argv[2]
outp = sys.argv[3]

incpathlist = list()
excpathlist = list()

plf = open(poly)
pllis = plf.readlines()
plf.close()

for pol in pllis:
    if pol[0] == "-":
        include = False
    else:
        include = True
    pol = [float(val) for val in pol.strip().strip(")").split("(")[1].split(",")]
    firstlist = pol[::2]
    secondlist = pol[1::2]
    pthlist = list()
    for num, val in enumerate(firstlist):
        if val != 0 and secondlist[num] != 0.:
            pthlist.append([val, secondlist[num]])
    if include:
        incpathlist.append(path.Path(pthlist))
    else:
        excpathlist.append(path.Path(pthlist))

eventlist = pf.open(evli)
bclis = eventlist[1].data.field('BETA_CORR')
pilis = eventlist[1].data.field('PI')
bepi_combs = np.array([bclis,pilis]).transpose()

sellist = incpathlist[0].contains_points(bepi_combs)
for ip in incpathlist[1:]:
    sellist += ip.contains_points(bepi_combs)

msklist = excpathlist[0].contains_points(bepi_combs)
for ep in excpathlist[1:]:
    msklist += ep.contains_points(bepi_combs)

sellist = sellist * (-msklist)

eventlist[1].data = eventlist[1].data[sellist]

eventlist.writeto(outp)

eventlist.close()
