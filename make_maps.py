#!/usr/bin/python
"""
This script will make maps from fits. 	
"""
import math
import numpy
import string
import pyfits
import sys

def main():
    
# Declare values and arrays
    # Values
    kpc=3.086E+19 	# meter
    scale=41.56		# 1 arcmin in kpc
    pixsize=4./60.	# in arcmin
    kbkev=8.617343E-8
    kberg=1.380658E-16
    try:
        fitsfile=sys.argv[1]
    except:
	print __doc__

    clob="clobber" in sys.argv[1:]
    
    f=open('surface.txt','r')
    filecontents=f.readlines()
    f.close()
    num=len(filecontents)-1
    
    # Arrays
    bins=numpy.arange(num)
    surf=numpy.arange(num)
    norm=numpy.arange(num, dtype=float)
    temp=numpy.arange(num, dtype=float)
    sigm=numpy.arange(num, dtype=float)  
    abfe=numpy.arange(num, dtype=float)
    entr=numpy.arange(num, dtype=float)
    pres=numpy.arange(num, dtype=float)
    dens=numpy.arange(num, dtype=float)
    cstat=numpy.arange(num, dtype=float)
	
    enorm=numpy.arange(num, dtype=float)
    etemp=numpy.arange(num, dtype=float)
    esigm=numpy.arange(num, dtype=float)  
    eabfe=numpy.arange(num, dtype=float)
    eentr=numpy.arange(num, dtype=float)
    epres=numpy.arange(num, dtype=float)
    edens=numpy.arange(num, dtype=float)
    
	
   
# Read surface values for each bin
    for line in filecontents:
        column=string.split(line)
	if (int(column[0])!=0):
	   k=int(column[0])-1
	   surf[k]=float(column[1])
    
# Read fitted values for each bin       
    for i in bins:
        filename='errors_'+str(i+1)+'.out'
	f=open(filename,'r')
	k=0
        for j in f.readlines():
	    if "Errors:" in j:
                 column=string.split(j)
	         if (k==0):
	             norm[i]=float(column[4])
		     enorm[i]=max(abs(float(column[6])),abs(float(column[8])))   
	         if (k==1):
	             temp[i]=float(column[5])
		     etemp[i]=max(abs(float(column[7])),abs(float(column[9])))   
	         if (k==2):
	             abfe[i]=float(column[5])
		     eabfe[i]=max(abs(float(column[7])),abs(float(column[9])))   
	         if (k==3):
	             sigm[i]=float(column[5])
		     esigm[i]=max(abs(float(column[7])),abs(float(column[9])))   
	         k=k+1
	    if ("C-statistic" in j) and ("Fit met" not in j):
		     column=j.split(":")
		     cstat[i]=float(column[1])

	f.close()
# Calculate pressure and entropy
    for i in bins:
        vol=surf[i]*((pixsize*scale*kpc)**2)*1000*kpc
        dens[i]=math.sqrt(norm[i]*1.E+64/(1.195*vol))   # In m^{-3}
	dens[i]=dens[i]/1.E+6 				# In cm^{-3}
        #temp[i]=wdem2temp(temp[i],0.02,wdem[i],0.02,0.1,0.01)[0]
	entr[i]=temp[i]*dens[i]**(-2/3)
        pres[i]=temp[i]*dens[i]*kberg/kbkev
	eentr[i]=(etemp[i]/temp[i])**2 + (1/6.)*(enorm[i]/norm[i])**2
	eentr[i]=math.sqrt(eentr[i])
	epres[i]=(etemp[i]/temp[i])**2 + (1/4.)*(enorm[i]/norm[i])**2
	epres[i]=math.sqrt(epres[i])
	
# Make Normalisation map
    
    fits=pyfits.open(fitsfile)
    
    normimg=fits[0].data
    shape=normimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(normimg[i][j])
	    if (binnr>0):
	       #print binnr
	       normimg[i][j]=math.sqrt(norm[binnr-1]/1.195)
    
    fits[0].data=normimg
    fits[0].writeto('normmap.fits',clobber=clob) 


# Make Sigma map    
       
    fits=pyfits.open(sys.argv[1])
    
    tempimg=fits[0].data
    shape=tempimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(tempimg[i][j])
	    if (binnr>0):
	       #print binnr
	       tempimg[i][j]=sigm[binnr-1]
    
    fits[0].data=tempimg
    fits[0].writeto('sigmap.fits',clobber=clob)  
    
    fits.close()
    
# Make Temperature map    
       
    fits=pyfits.open(sys.argv[1])
    
    tempimg=fits[0].data
    shape=tempimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(tempimg[i][j])
	    if (binnr>0):
	       #val=wdem2temp(temp[binnr-1],0.02,wdem[binnr-1],0.02,0.1,0.01)
	       tempimg[i][j]=temp[binnr-1]
    
    fits[0].data=tempimg
    fits[0].writeto('tempmap.fits', clobber=clob)  
    
    fits.close()
    
# Make Fe map    
    
    fits=pyfits.open(sys.argv[1])
    
    abfeimg=fits[0].data
    shape=abfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(abfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       abfeimg[i][j]=abfe[binnr-1] 	       
        
    
    fits[0].data=abfeimg
    fits[0].writeto('femap.fits',clobber=clob)
    fits.close()
        
# Make Entropy map    
    
    fits=pyfits.open(sys.argv[1])
    
    abfeimg=fits[0].data
    shape=abfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(abfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       abfeimg[i][j]=entr[binnr-1] 	       
        
    
    fits[0].data=abfeimg
    fits[0].writeto('entrmap.fits',clobber=clob)
    fits.close()
        
# Make pressure map    
    
    fits=pyfits.open(sys.argv[1])
    
    abfeimg=fits[0].data
    shape=abfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(abfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       abfeimg[i][j]=pres[binnr-1] 	       
        
    
    fits[0].data=abfeimg
    fits[0].writeto('presmap.fits',clobber=clob)

    fits.close()

# make C-stat map
    fits=pyfits.open(sys.argv[1])
    
    abfeimg=fits[0].data
    shape=abfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(abfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       abfeimg[i][j]=cstat[binnr-1] 	       
        
    
    fits[0].data=abfeimg
    fits[0].writeto('cstatmap.fits',clobber=clob)

    fits.close()
    
    # Make Normalisation error map
    
    fits=pyfits.open(fitsfile)
    
    enormimg=fits[0].data
    shape=enormimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(enormimg[i][j])
	    if (binnr>0):
	       #print binnr
	       #print enorm[binnr-1]
	       enormimg[i][j]=math.sqrt(enorm[binnr-1]/norm[binnr-1])
	       
    
    fits[0].data=enormimg
    fits[0].writeto('enormmap.fits',clobber=clob) 


# Make Sigma error map    
       
    fits=pyfits.open(sys.argv[1])
    
    etempimg=fits[0].data
    shape=etempimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(etempimg[i][j])
	    if (binnr>0):
	       #print binnr
	       if sigm[binnr-1] != 0:
	            etempimg[i][j]=esigm[binnr-1]/sigm[binnr-1]
	       else:
		    etempimg[i][j]=0.
	       
    
    fits[0].data=etempimg
    fits[0].writeto('esigmap.fits',clobber=clob)  
    
    fits.close()
    
# Make Temperature error map    
       
    fits=pyfits.open(sys.argv[1])
    
    etempimg=fits[0].data
    shape=etempimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(etempimg[i][j])
	    if (binnr>0):
	       #val=wdem2temp(temp[binnr-1],0.02,wdem[binnr-1],0.02,0.1,0.01)
	       etempimg[i][j]=etemp[binnr-1]/temp[binnr-1]
    
    fits[0].data=etempimg
    fits[0].writeto('etempmap.fits', clobber=clob)  
    
    fits.close()
    
# Make Fe error map    
    
    fits=pyfits.open(sys.argv[1])
    
    eabfeimg=fits[0].data
    shape=eabfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(eabfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       eabfeimg[i][j]=eabfe[binnr-1]/abfe[binnr-1] 	       
        
    
    fits[0].data=eabfeimg
    fits[0].writeto('efemap.fits',clobber=clob)
    fits.close()
        
# Make Entropy error map    
    
    fits=pyfits.open(sys.argv[1])
    
    eabfeimg=fits[0].data
    shape=eabfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(eabfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       eabfeimg[i][j]=eentr[binnr-1]
        
    
    fits[0].data=eabfeimg
    fits[0].writeto('eentrmap.fits',clobber=clob)
    fits.close()
        
# Make pressure error map    
    
    fits=pyfits.open(sys.argv[1])
    
    eabfeimg=fits[0].data
    shape=eabfeimg.shape
          
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
	    binnr=int(eabfeimg[i][j])
	    if (binnr>0):
	       #print binnr
	       eabfeimg[i][j]=epres[binnr-1] 	       
        
    
    fits[0].data=eabfeimg
    fits[0].writeto('epresmap.fits',clobber=clob)

    fits.close()


#def wdem2temp(tmax,dtmax,p,dp,b,db):
    
    #temp=numpy.arange(2, dtype=float)    
    
    #a  = 1./p
    #da = dp/p * a

    #t = (1.+a)/(2.+a) * (1.-b**(a+2.))/(1.-b**(a+1.)) * tmax

    #dtda = tmax * ( (1.+a)/(2.+a) * math.log10(b) * (b**(1.+a) - b**(2.+a))/(1.-b**(1.+a))**2. + 1./(2.+a)**2. * (1.-b**(2.+a))/(1.-b**(1+a)) )

    #dtdb = tmax * ( (1.+a)/(2.+a) * (a*b**a + b**a - a*b**(a+1.) - 2.*b**(a+1.) + b**(2.*a+2.))/(1.-b**(1.+a))**2. )

    #dtdt = (1.+a)/(2.+a) * (1.-b**(a+2.))/(1.-b**(a+1.))

    #dt = math.sqrt((dtda * da)**2. + (dtdb * db)**2. + (dtdt * dtmax)**2.)

    #temp[0]=t
    #temp[1]=dt

    #return temp


main()
