#!/usr/bin/python

""" This script will load the data from a histogram given as command line input. It will fitt a poissonian to it and show you the results. Upon acceptance, a GTI file will be created. """
import sys
import pyfits as pf
import scipy.stats as ns
from numpy import *
#from pylab import *
import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import scipy as sp
from math import sqrt
import time as tm
import os

def comp(a,b):
	for val in a:
		if val in b:
			return True
	return False

yesarr=["yes","y", "1"]
noarr=["no","n","0", "n0"]
helparr=["h","help","-h"]

if comp(sys.argv[1:],helparr):
	print __doc__
	sys.exit(0)

nsig=2 # Change if you want to cut more or less sigmas.
init_val=7
flag=False

filebase='.'.join((sys.argv[1].split("."))[:-1])
fl = pf.open(sys.argv[1])
dataX=(fl[1].data.field('X'))[1:]
dataY=(fl[1].data.field('Y'))[1:]


norm = sum(dataY)

fitfunc = lambda p,x: p[1]*ns.poisson.pmf(x,p[0])
#errfunc = lambda p,x,y: (fitfunc(p,x) - y)
errfunc = lambda p,x,y: ((fitfunc(p,x) - y)**2).sum()

pl.ion()

while not flag:
	pinit=[init_val, norm]

	#out = sp.optimize.leastsq(errfunc, pinit[:], args=(dataX,dataY))
	out = sp.optimize.fmin(errfunc, pinit[:], args=(dataX,dataY))

	folders=os.getcwd().lower().split("/")
	
	if comp(["m1","mos1"],folders):
		detstring="detector MOS 1"	
	elif comp(["m2","mos2"],folders):
		detstring="detector MOS 2"
	elif ("pn" in folders):
		detstring="detector pn"
	else:
		detstring="path="+os.getcwd()
	
	
	
	ot=[ceil(out[0]),out[1]]
	
	pl.plot(dataX,dataY,"bo", dataX,fitfunc(ot, dataX), "b-")

	pl.suptitle("Best fit poissonian to histogram data, mu="+str(int(ot[0])))
	pl.title(detstring+"  "+"obs. id: "+filebase)
	pl.xlabel("number of counts")
	pl.ylabel("number of bins")
	pl.legend(('data', 'poissonian'))

	ax = pl.axes()
	ax.set_xlim(max(0,ot[0]-(nsig+2)*sqrt(ot[0])),ot[0]+(nsig+2)*sqrt(ot[0]))
	
	print "best-fit value:  "+str(out[0])+"("+str(ot[0])+")"


	pl.draw()
		
	answ=raw_input("do you agree with the fitted value?  ")

	if answ.lower() not in noarr:
		print "You didn't say no, i'll assume that you mean it's ok"
		flag=True
	else:
		init_val=raw_input("Give initial guess for next fitting step:  ")
		

if flag:
	print "Fit succeeded!"
else:
	print "This should not happen"
	sys.exit(2)

hi=ceil(ot[0]+nsig*sqrt(ot[0]))
lo=floor(ot[0]-nsig*sqrt(ot[0]))
out=filebase+".gti"
lcve=filebase+".lcve"
print "GTI will be made cutting data between "+str(lo)+" and "+str(hi)
sysvar="tabgtigen table="+lcve+" expression=\"COUNTS in ["+str(lo)+":"+str(hi)+"] \" gtiset="+out
os.system("source ~/bin/sasinit && "+sysvar)

a=open("fitted_values","a")
a.write(tm.asctime()+": \t"+out+"\t"+str(ot[0])+"\t"+str(lo)+"\t"+str(hi)+"\n")
a.close()


