#!/usr/bin/python
"""

script to add region information to a spectrum extracted using evselect.py.
Input parameters are the masterfile containing the region and the input file.
Script checks whether there are already one or more regions present and overwrites output

"""
def comp(a,b):
        for val in a:
                if val in b:
                        return True
        return False

import sys

if len(sys.argv) < 3 or comp(['h','help','-h','--help'],sys.argv[1:]):
	print __doc__
	sys.exit(0)

import pyfits as pf
import os



master=sys.argv[1]
target=sys.argv[2]+".old"
outfile="tmp.fits"




os.system("mv "+sys.argv[2]+" "+sys.argv[2]+".old")

ma=pf.open(master)
tg=pf.open(target)

reglis=list()
for i in range(len(tg)):
     if "REG" in tg[i].name:
	reglis.append(i)

reglis.reverse()

if len(reglis) is 1:
	print "this file already contains reg list. Quitting"
elif len(reglis) > 1:
	print "More than one reg list. Removing all but first."
	for i in reglis[:-1]:
		del tg[i]
else:
	
	tg.append(ma[3])
	
	if tg[1].header['INSTRUME'] == "EPN":
		nd=str(4)
	else:
		nd=str(5)

	i=0
	for k in ma[1].header.ascardlist().keys():
		if "DSTYP" in k:
			i+=1
	
	i=str(i)
	tg[1].header.update('DSTYP'+i,ma[1].header['DSTYP'+nd])
	tg[1].header.update('DSVAL'+i,ma[1].header['DSVAL'+nd])
	tg[1].header.update('DSREF'+i,ma[1].header['DSREF'+nd])

tg.writeto(outfile,clobber=True)
os.system("mv "+outfile+" "+sys.argv[2])
os.system("rm -f "+outfile)
os.system("rm -f "+target)
	