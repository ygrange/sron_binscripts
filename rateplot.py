#!/usr/bin/python

import pyfits
from plplot import *
import sys

fn=sys.argv[1]

fle=pyfits.open(fn)

xdat=fle[1].data.field('TIME')
ydat=fle[1].data.field('RATE')
fle.close()
	
xdat2=list()

for i in xdat:
	xdat2.append(i-xdat[0])

plinit()

plenv(min(xdat2), max(xdat2), min(ydat), max(ydat),0,1)
pllab("TIME","RATE","title")
plline(xdat2,ydat)
raw_input()
