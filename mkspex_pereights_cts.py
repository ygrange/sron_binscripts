#!/usr/bin/python

import os,sys

fnam=sys.argv[1]
r=int(sys.argv[2])

if r>2 or r<1:
	sys.stderr.write("RGS number can't be more than 2 or less than 1.")
	sys.exit(2)

R=dict()

R["111"]=[35.9,38.2]
R["112"]=[33.6,35.9]
R["121"]=[31.25,33.5]
R["122"]=[29.0,31.25]
R["131"]=[26.85,28.9]
R["132"]=[24.8,26.85]
R["141"]=[22.8,24.7]
R["142"]=[20.9,22.8]
R["151"]=[19.0,20.8]
R["152"]=[17.2,19.0]
R["161"]=[15.75,17.1]
R["162"]=[13.8,15.75]
R["171"]=[12.2,13.8]
R["172"]=[10.6,12.2]
R["181"]=[9.2,10.6]
R["182"]=[7.8,9.2]
R["191"]=[6.45,7.7]
R["192"]=[5.2,6.45]
R["211"]=[35.0,37.3]
R["212"]=[32.7,35.0]
R["221"]=[30.4,32.6]
R["222"]=[28.2,30.4]
R["231"]=[26.1,28.1]
R["232"]=[24.0,26.1]
R["241"]=[22.05,24.0]
R["242"]=[20.1,22.05]
R["251"]=[18.25,20.0]
R["252"]=[16.5,18.25]
R["261"]=[14.8,16.4]
R["262"]=[13.2,14.8]
R["271"]=[11.55,13.1]
R["272"]=[10.0,11.55]
R["281"]=[8.6,10.0]
R["282"]=[7.2,8.6]
R["291"]=[5.95,7.2]
R["292"]=[4.7,5.95]

spectrum=list()
spectrum.append("READ Terr 1 2\n")
if r==1:
   skip=7
if r==2:
   skip=4

for c in range(9,0,-1):
        if c==skip:
           continue
	for h in [2,1]:
		id="%i%i%u"%(r,c,h)
		ubd=R[id][1]
		lbd=R[id][0]
		delta=(ubd-lbd)/4.
		b1=[lbd,lbd+delta]
		b2=[b1[1],b1[1]+delta]
		b3=[b2[1],b2[1]+delta]
		b4=[b3[1],b3[1]+delta]
		wf=open("cmdfil.com","w")
		wf.write(""                                       \
		"data %s %s \n"                                   \
		"pl dev null \n"                                  \
		"pl ty dat \n"                                    \
		"pl ux a \n"                                      \
		"pl uy co \n"                                     \
		"plot \n"                                         \
		"ign 0:%s un a \n"                                \
		"ign %s:10000000 un a \n"                         \
		"bin 0:100000000000000000000000 1000000 u a \n"   \
		"plot adum firstbin ov \n"                        \
		"use 0:100000 un a \n"                            \
		"ign 0:%s un a \n"                                \
		"ign %s:10000000 un a \n"                         \
		"bin 0:100000000000000000000000 1000000 u a \n"   \
		"plot adum secondbin ov \n"                       \
		"use 0:100000 un a \n"                            \
		"ign 0:%s un a \n"                                \
		"ign %s:10000000 un a \n"                         \
		"bin 0:100000000000000000000000 1000000 u a \n"   \
		"plot adum thirdbin ov \n"                        \
		"use 0:100000 un a \n"                            \
		"ign 0:%s un a \n"                                \
		"ign %s:10000000 un a \n"                         \
		"bin 0:100000000000000000000000 1000000 u a \n"   \
		"plot adum fourthbin ov \n"			  \
		"quit \n"                                         \
		""%(fnam,fnam,b1[0],b1[1],b2[0],b2[1],b3[0],b3[1],b4[0],b4[1]))
		wf.close()
		os.system("spex<<EOF \n l e cmdfil \n EOF")
		
		for fi in ["first","second","third","fourth"]:
			fh=open(fi+"bin.qdp")
			dd=fh.readlines()
			if len(dd)>0:
				spectrum.append(dd[-1])
			fh.close()
			os.remove(fi+"bin.qdp")
		os.remove("cmdfil.com")

wf=open(fnam+"_cts.qdp","w")
wf.writelines(spectrum)
wf.close()
