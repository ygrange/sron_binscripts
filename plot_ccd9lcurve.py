#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
import sys
from numpy import array
from numpy import where,intersect1d,union1d, Inf,intersect1d_nu
from os import getcwd

def histo(ary,width=100.):
	"""Very simple histogramming algorithm. The array [ary] is binned using a bin width of [width]. The return is a two dimensional numpy array containing bin average values and counts in each bin. No weighting is applied."""
	ary=array(ary)
	ary.sort()
	lbd=ary[0]
	ubd=ary[0]+width
	maxi=ary[-1]
	flag=True
	
	xar=list()
	yar=list()
	
	while(flag):
		ll=where(ary>=lbd)[0]
		uu=where(ary<ubd)[0]
		selar=intersect1d(uu,ll)
		
		xar.append(((ubd-lbd)/2.)+lbd)
		yar.append(len(selar))
		lbd+=width
		ubd+=width
		if (lbd>maxi):
			flag=False
		
	return array([xar,yar])
	
	
def factors(ary):

	bounds=[0.01,
	0.02,
	0.04,
	0.06,
	0.08,
	0.1,
	0.2,
	0.4,
	0.6,
	0.8,
	1.,
	2.,
	4.,
	6.,
	8.]
#
	lbds=bounds[:]
	lbds.insert(0,-Inf)
	ubds=bounds[:]
	ubds.append(Inf)
	nums=[0.]*len(lbds)
#		
	for i in range(len(lbds)):
		tmpa=ary[ary>=lbds[i]]
		tmpa=tmpa[tmpa<ubds[i]]
		nums[i]=float(len(tmpa))
	
	nums=array(nums)
	facs=nums/sum(nums)
	
	lbds=array(lbds)
	ubds=array(ubds)
	midvals=((ubds-lbds)/2)+lbds
	midvals[0]=lbds[1]*0.5
	midvals[-1]=ubds[-2]*2.
	
	tmpa=midvals*facs
	ff=sum(tmpa)
	
	return ff,facs
	

fl=sys.argv[1]

if "R1" in fl or "r1" in fl:
	rgs=1
elif "R2" in fl or "r2" in fl:
	rgs=2
else:
	rgs=int(raw_input("Which RGS are we plotting? "))
	
try:
	par=sys.argv[2]
	spp=par.split("=")
	if spp[0] == "width" or spp[0] == "w":
		width=float(spp[1])
	else:
		width=100.
		sys.stderr.write("width=100\n")
except:
	width=100.
	sys.stderr.write("width=100\n")
		
			
## Only works if file has normal EVLI type name. Could be junk if not.


oid=fl[1:11]

sys.stderr.write("=============\n")
sys.stderr.write(getcwd()+"/"+fl+"\n")
sys.stderr.write("=============\n")
fh=pf.open(fl)
CCDNR=fh[1].data.field('CCDNR')
XDSP=fh[1].data.field('XDSP_CORR')
FLAG=fh[1].data.field('FLAG')
TIME=fh[1].data.field('TIME')
extime=fh[1].header['ONTIME']
fh.close()

idc=where(CCDNR==9)[0]
# select ccdnr == 9
idf8=where(FLAG==8)
idf16=where(FLAG==16)
idf=union1d(idf8[0],idf16[0])
idf=where(FLAG<1e100)[0]
# select flag == 8 or flag == 16
idxl=where(XDSP<-0.0003)
idxh=where(XDSP>0.0003)
idx=union1d(idxl[0],idxh[0])
# select xdsp_corr <= -3.e-4 or xdsp_corr >= 3.e-4
id=intersect1d(idc,idf)
id=intersect1d(id,idx)

lcdat=histo(TIME[id],width)

pltim=lcdat[0]
plcts=lcdat[1]/width

a,b=factors(plcts)

print oid, "R"+str(rgs), a

pl.ion()
pl.suptitle("CCD 9, |XDSP|>3.e-4 light curve")
pl.title("RGS"+str(rgs)+", obs. id: "+oid)

pl.xlabel("Time")
pl.ylabel("Counts/s")
pl.plot(pltim,plcts)

#pl.draw()

#raw_input("press any key to continue.")

pl.savefig('.'.join(fl.split(".")[:-1])+".ps")
