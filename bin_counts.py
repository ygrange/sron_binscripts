#! /usr/bin/env python
import pyfits
import sys
from pyclus import A2keV

if len(sys.argv) == 3:
    sys.stderr.write("Assuming you want to use the full spectral range available. If you don't, rerun the script using the second and third command line parameter as minimum and maximum values (in Angstrom) \n")
    limits = False

elif len(sys.argv) == 5:
    underlim = A2keV(float(sys.argv[4]))
    upperlim = A2keV(float(sys.argv[3]))
    limits = True
else:
    print "Usage: %s spo_file.spo CPB [Under_limit] [Upper_limit] where spo_file.spo is the spex spo format file, CPB is the number of counts per bin and the under and upper limit are in Angstrom. If no limits are specified, the full available range is used"
    sys.exit(1)

filename = sys.argv[1]
if filename[-4:] != ".spo":
    filename += ".spo"

CPB = int(sys.argv[2])

fh = pyfits.open(filename)

dataset = fh[2].data

counter = 0

if limits:
    while dataset[counter]['Lower_Energy'] < underlim:
        counter += 1
else:
    upperlim = 100000.

countsum = 0
elow = dataset[counter]['Lower_Energy']
eup = elow

#print underlim, upperlim

for frame in dataset[counter:]:
    if frame['Upper_Energy'] > upperlim:
        print "bin %s:%s 100000 u kev \n# %s (last bin)" % (elow, eup, countsum)
	break

    if countsum >= CPB:
        print "bin %s:%s 100000 u kev \n# %s" % (elow, eup, countsum)
	countsum = 0
	elow = frame['Lower_Energy']
	
    if frame['Lower_Energy'] != eup and countsum != 0:
        # This is a hole, let's just bin around it.
	pass
	#print "bin %s:%s 100000 u kev # %s (hole)" % (elow, eup, countsum)
	#countsum = 0
	#elow = frame['Lower_Energy']

    
    counts = (frame['Source_Rate'] + frame['Back_Rate']) * frame['Exposure_Time']
    countsum += counts
    eup = frame['Upper_Energy']
else:
    print "bin %s:%s 100000 u kev \n# %s (last bin)" % (elow, eup, countsum)        

#print counter

#(fle[2].data[0]['Source_Rate']+fle[2].data[0]['Back_Rate'])*fle[2].data[0]['Exposure_Time']

