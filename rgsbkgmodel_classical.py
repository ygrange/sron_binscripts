#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
import sys
from datetime import datetime
from numpy import array, sqrt
from numpy import where,intersect1d,union1d, Inf,intersect1d_nu
import os
import string as st

try:
	SAS_CCFPATH=os.environ["SAS_CCFPATH"]
except:
	SAS_CCFPATH="/home/sas/currentccf"
	sys.stderr.write("no CCFPATH parameter found. Will assume SRON default value")
try:
	SAS_CCF=os.environ["SAS_CCF"]
except:
	sys.stderr.write("SAS_CCF parameter not found. I will not accept this")
	sys.exit(2)

def histo(ary,width=100,starts=False,stops=False):
	"""Very simple histogramming algorithm. The array [ary] is binned using a bin width of [width]. The return is a two dimensional numpy array containing bin average values and counts in each bin. No weighting is applied."""
	ary=array(ary)
	ary.sort()
	lbd=ary[0]
	ubd=ary[0]+width
	maxi=ary[-1]
	flag=True
	
	xar=list()
	yar=list()
	
	ii=0
	while(flag):
		ll=where(ary>=lbd)[0]
		uu=where(ary<ubd)[0]
		selar=intersect1d(uu,ll)
		if type(starts) is bool:
			xar.append(((ubd-lbd)/2.)+lbd)
			yar.append(len(selar))
		else:
			while (ubd>stops[ii]) and (ii<(len(starts)-1)):
				ii+=1
			if lbd>starts[ii] and ubd<stops[ii]:
				xar.append(((ubd-lbd)/2.)+lbd)
				yar.append(len(selar))
		lbd+=width
		ubd+=width
		if (lbd>maxi):
			flag=False
		
	return array([xar,yar])
	
	
def factors(ary):
	""" Function to return the factors based on the BLI for XMM rgsbkgmodel. The output consists of two parts. The first one is the waighted averaged BLI based on the bins defined by Gonzales-Riestra (2005) and the factors derived. The second element is the list of factors itself."""
	bounds=[0.01,
	0.02,
	0.04,
	0.06,
	0.08,
	0.1,
	0.2,
	0.4,
	0.6,
	0.8,
	1.,
	2.,
	4.,
	6.,
	8.]
#
	lbds=bounds[:]
	lbds.insert(0,-Inf)
	ubds=bounds[:]
	ubds.append(Inf)
	nums=[0.]*len(lbds)
#		
	for i in range(len(lbds)):
		## lower bound inclusive, upper bound exclusive
		tmpa=ary[ary>=lbds[i]]
		tmpa=tmpa[tmpa<ubds[i]]
		
		## lower bound exclusive, upper bound inclusive (seems to make more
		## sense since lowest bound essentially is 0. counts so lower inclusive
		## would mean all the background contributed by times in the light curve
		## with 0 counts, which essentially is a zero bkg... 
		#tmpa=ary[ary>lbds[i]]
		#tmpa=tmpa[tmpa<=ubds[i]]

		nums[i]=float(len(tmpa))
		
	
	nums=array(nums)
	facs=nums/sum(nums)
	
	lbds=array(lbds)
	ubds=array(ubds)
	midvals=((ubds-lbds)/2)+lbds
	midvals[0]=lbds[1]*0.5
	midvals[-1]=ubds[-2]*2.
	
	tmpa=midvals*facs
	ff=sum(tmpa)
	
	return ff,facs
	
def bkgcal(facts,rgs,order,pdist,binning):
	
	
	pdist=int(pdist)
	
	ccf=pf.open(SAS_CCF)
	ccfdat=ccf[1].data
	instr=ccfdat.field("SCOPE")
	typeid=ccfdat.field("TYPEID")
	file=ccfdat.field("FNAME")
	ccf.close()
	
	selin=typeid[where(instr=="RGS"+str(rgs))]
	selfi=file[where(instr=="RGS"+str(rgs))]
	cfile=(selfi[where(selin=="TEMPLATEBCKGND")])[0]
			
	## No loops over files anymore. Finding the bkg based
	## on the ccf file is much better.
	#for c in ccfiles:
		#if int(c[-5])>i and int(c[-5])<8:
			#i=int(c[-5])
			#cfile=c
	calfile=SAS_CCFPATH.rstrip("/")+"/"+cfile
	
	ch=pf.open(calfile)
	
	left="X100_P"+str(pdist).zfill(3)+"_"+str(order)+"_" 
	right="_"+st.upper(binning[0])
	
	datholder=dict()
	uselist=list()
	binvals=list()
	expos=list()
	for c in ch:
		if left in c.name and right in c.name:
			uselist.append(ch.index(c))
			datholder[c.name[-6:-2]]=c.data
			binvals.append(c.name[-6:-2])
			expos.append(c.header['EXPOSURE'])
	ch.close()
	binvals.sort()
	
	if len(binvals) is not len(facts):
		sys.stderr.write("Number of factors and number of bkg templates differ. This really should not happen!\n")
		sys.stderr.write(str(len(facts))+" factors vs "+str(len(binvals))+" bkg templates\n")
		sys.exit(2)
		
	
	facts=array(facts)
	expos=array(expos)
	
	
	mx=facts.argmax()
	
	quality=datholder[binvals[mx]].field("QUALITY")[0]
	areascal=datholder[binvals[mx]].field("AREASCAL")[0]
	backscal=datholder[binvals[mx]].field("BACKSCAL")[0]
	
	trat=array([0.]*len(quality))
	terrat=array([0.]*len(quality))
	
	for vv in range(len(binvals)):
		cts=datholder[binvals[vv]].field("COUNTS")[0]
		chan=datholder[binvals[vv]].field("CHANNEL")[0]
		are=datholder[binvals[vv]].field("AREASCAL")[0]
		rat=cts/(are*expos[vv])
		trat+=rat*facts[vv]
		## START CHECK
		errat=sqrt(cts)/(are*expos[vv])
		terrat+=(errat**2)*(facts[vv]**2)
		## END CHECK
	terrat=sqrt(terrat)
	xp=(expos*facts).sum()
	num=len(where(areascal<0.6)[0])
	if num>0:
		sys.stderr.write("Putting "+str(num)+" areascals to 1 since they are under 0.6\n")
	areascal[where(areascal<0.6)]=1.
	
	return xp,trat,terrat,quality,areascal,backscal
	
fl=sys.argv[1]

if "R1" in fl or "r1" in fl:
	rgs=1
elif "R2" in fl or "r2" in fl:
	rgs=2
else:
	rgs=int(raw_input("Which RGS are we using? "))
	

parms=dict()
for par in sys.argv[1:]:
	spp=par.split("=")
	try:
		parms[spp[0]]=spp[1]
	except:
		parms["null"]="null"

try:
	width=float(parms["width"])
except:
	width=100.
	sys.stderr.write("width=100\n")
try:
	gti=parms["gti"] 
	gtifile=True
except:
	gtifile=False
try:
	order=parms["order"]
except:
	order=1
	sys.stderr.write("Assuming first order!\n")
try:
	pdistfac=float(parms["pdistincl"])/100.
except:
	pdistfac=0.95
	sys.stderr.write("Assuming pdistincl=95!\n")
try:
	binning=parms["binning"]
	if binning not in ["beta","lambda"]:
		sys.stderr.write("Binning is either lambda or beta!\n")
		sys.exit(2)
except SystemExit:
	sys.exit(2)
except:
	binning="beta"
	sys.stderr.write("Assuming betabinning!\n")
try:
	cal=bool(parms["cal"])
except:
	cal=False
				
				
## Only works if file has normal EVLI type name. Could be junk if not.


oid=fl[1:11]

sys.stderr.write("=============\n")
sys.stderr.write(os.getcwd()+"/"+fl+"\n")
sys.stderr.write("=============\n")
if not gtifile:
	sys.stderr.write("No gti file input. Will assume that all time is used. This may result in an over estimate of the fraction in the lowest bin (i.e. bins with 0-1 counts/100s)!\n")
	starts=False
	stops=False
else:
	gf=pf.open(gti)
	starts=gf[1].data.field("START")
	stops=gf[1].data.field("STOP")
	gf.close()

fh=pf.open(fl)
CCDNR=fh[1].data.field('CCDNR')
XDSP=fh[1].data.field('XDSP_CORR')
FLAG=fh[1].data.field('FLAG')
TIME=fh[1].data.field('TIME')
extime=fh[1].header['ONTIME']
fh.close()


## select ccdnr == 9
idc=where(CCDNR==9)[0]

## select flag == 8 or flag == 16
idf8=where(FLAG==8)
idf16=where(FLAG==16)
idf=union1d(idf8[0],idf16[0])
## Or select as it is now, on all flags (just uncomment, will overwrite the older one).
idf=where(FLAG<Inf)[0]
## select xdsp_corr <= -3.e-4 or xdsp_corr >= 3.e-4
idxl=where(XDSP<-0.0003)
idxh=where(XDSP>0.0003)
idx=union1d(idxl[0],idxh[0])

id=intersect1d(idc,idf)
id=intersect1d(id,idx)

lcdat=histo(TIME[id],width,starts,stops)

pltim=lcdat[0]
plcts=lcdat[1]/width

if not cal:
	tf,facts=factors(plcts)
else:
	facts=array(16*[0.])
	for v in range(len(facts)):
		facts[v]=float(raw_input("What is factor "+str(v+1)+"?  "))
		tf="CAL"


xp,rat,errat,qual,are,back=bkgcal(facts,rgs,order,100.*pdistfac,binning)


bf=fl.replace("EVENLI","MBSPEC")
bf=bf[:-8]+str(order)+bf[-7:]

bh=pf.open(bf)
for ii in range(len(facts)):
	bh[2].data.field("FACTORS")[ii]=facts[ii]

for jj in range(len(rat)):
	bh[1].data.field("RATE")[jj]=rat[jj]
	bh[1].data.field("STAT_ERR")[jj]=errat[jj]
	bh[1].data.field("QUALITY")[jj]=qual[jj]
	bh[1].data.field("BACKSCAL")[jj]=back[jj]
	bh[1].data.field("AREASCAL")[jj]=are[jj]

bh[1].header["EXPOSURE"]=xp
dtt=datetime.now().isoformat().split(".")[0]
bh[1].header.add_history("Redirived the background with YSAS-bg. "+dtt)
bh[2].header.add_history("Redirived the background with YSAS-bg. "+dtt)

bh.writeto(bf.replace("MBSPEC","YGSPEC"),clobber=True)
	
#pl.ion()
#pl.suptitle("CCD 9, |XDSP|>3.e-4 light curve")
#pl.title("RGS"+str(rgs)+", obs. id: "+oid)

#pl.xlabel("Time")
#pl.ylabel("Counts/s")
#pl.plot(pltim,plcts)

#pl.draw()

#raw_input("press any key to continue.")

#pl.savefig('.'.join(fl.split(".")[:-1])+".ps")

print oid,tf