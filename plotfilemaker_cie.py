#!/usr/bin/python

"""
CIE version!
script to make ps files of all radial profiles. This script loops over all parameters found in the file named by variable [dicname] and makes postscript plots of the parameters in the range specified in the [rge] parameter. The WDEM cutoff factor is taken according to the [cut] parameter. The upper and lower boundaries of the annuli are taken according to the [annul] parameter. The files read in have the form of [prefix]i[postfix] where i is the number of the annulus (which is taken from the [rge] array. The spex command file should conatain the line "this is error on {par}", where {par} is the name of the parameter, just before error calculation. This will generate an error in the output file which is used by the script to grab the parameter and error values. The output files will be called [output_pre]var_name[output_post, where var_name is the name of the variable as defined in the dictionary file.The output file for ps will be [output_pre]var_name.ps.


Default variable values:
   rge=[1,2,3,4,5,6]
   cut=0.18
   dicname="dictionary.yan"
   annul=[0.,0.5,1.,2.,3.,4.,6.]
   large_annul=[0.,3.]
   prefix="annul_"
   postfix=".out"
   output_pre="plot_"
   output_post=".gp"

To change these parameters, just add a file called "cieplotrc" or "plotrc" to the folder of your data set. If "cieplotrc" exists, "plotrc" is ignored!

Dependencies:
	Pyclus
	Gnuplot python bindings

"""

import sys

variables=dict()
pubpars=list()

#if len(sys.argv)!=1:
	#print __doc__
	#sys.exit(0)
	
variables=dict()

try:
	f=open("cieplotrc")
	for entry in (f.readlines()):
		variables[entry.split("=")[0].strip()]=entry.split("=")[1].strip()
	f.close()
except:
	try:
		f=open("plotrc")
		for entry in (f.readlines()):
			variables[entry.split("=")[0].strip()]=entry.split("=")[1].strip()
		f.close()
	except:
		sys.stderr.write("no cieplotrc or plotrc found. All variables will be used as default.")	

for clv in sys.argv[1:]:
	if(clv=="h" or clv=="help" or clv=="-h" or clv =="--help"):
		print __doc__
		sys.exit(0)
	try:
		variables[clv.split("=")[0].strip().strip('"')]=clv.split("=")[1].strip().strip('"')
	except:
		pubpars.append(clv)

try:
	rnge=variables["rge"]
	rge=rnge[1:-1].split(",")
	for i in range(len(rge)):
		rge[i]=int(rge[i])
except:
	rge=[1,2,3,4,5,6]
try:
	annu=variables["annul"]
	annul=annu[1:-1].split(",")
	for i in range(len(annul)):
		annul[i]=float(annul[i])
except:
	annul=[0.,0.5,1.,2.,3.,4.,6.]
try:
	dicname=variables["dicname"]
except:
	dicname="dictionary.yan"
try:
	prefix=variables["prefix"]
except:
	prefix="annul_"
try:
	postfix=variables["postfix"]
except:
	postfix=".out"
try:
	output_pre=variables["output_pre"]
except:
	output_pre="plot_"
try:
	output_post=variables["output_post"]
except:
	output_post=".gp"


from pyclus import *
import Gnuplot

names=dict()

dicto=open(dicname)

for entry in (dicto.readlines()):
	names[entry.split(",")[0]]=entry.split(",")[1].strip()

dicto.close()


dataholder=dict()
errorholder=dict()
minholder=list()
maxholder=list()
#for i in [1,2]:

for i in rge:
	
	minholder.append(str(annul[i-1]))
	maxholder.append(str(annul[i]))
	
	fh=open(prefix+str(i)+postfix)
	data=fh.readlines()
	fh.close()

	#print names['nh'],i
	
	flag=0

	for line in data:
		#print dataholder
		if "Input error:" in line:
			flag=1
			unit=line[30:].strip()
			if i==1:
				try:
					unam=names[unit]
					dataholder[unit]=list()
					errorholder[unit]=list()
					dataholder[unit].append(unam)
				except:
					sys.stderr.write("\nsomething went wrong with unit "+unit+", probably you tried finding errors for a non-existing variable. Skipping...\n\n")
		if flag==2 :
			if "Parameter" in line  and "," in line:
				lis=line.replace(",",":").replace("\n","").replace("Errors","").split(":")
				dataholder[unit].append(str(lis[1]))
				errorholder[unit].append(max(abs(float(lis[2])),abs(float(lis[3]))))
	
				#print i,unit,lis[1],max(abs(float(lis[2])),abs(float(lis[3]))	
		elif flag==1:
			flag=2
	
	#print dataholder['nh']

for u in dataholder:
	ufil=output_pre+u+output_post
	try:
		1/(float(u)-26.)
		writefile=open(ufil,"wr")
		writefile.write("# "+dataholder[u][0]+"\n")
		for v in range(1,len(dataholder[u])):
			writefile.write(str(float(dataholder[u][v])/dataholder["26"][v])+"\t"+str(errprop(dataholder[u][v],dataholder["26"][v],errorholder[u][v-1],errorholder["26"][v-1]))+"\t"+minholder[v-1]+"\t"+maxholder[v-1]+"\n")
	
		writefile.close()	
	except:
		writefile=open(ufil,"wr")
		writefile.write("# "+dataholder[u][0]+"\n")
		for v in range(1,len(dataholder[u])):
			writefile.write((str(dataholder[u][v])+"\t"+str(errorholder[u][v-1])+" \t"+minholder[v-1]+"\t"+maxholder[v-1]+"\n"))
	
		writefile.close()
		
for u in dataholder:
	ufil=output_pre+u+output_post
	upsfil=output_pre+u
	g=Gnuplot.Gnuplot()
	g('set title "'+dataholder[u][0]+'"')
	g.xlabel("radial distance (arcmin)")
	g('set term postscript enhanced')
	g('set output "'+upsfil+'.ps"')
	g('set size 0.6,0.6')
	g('set yrange [0:*]')
	g('plot "'+ufil+'" u ($3+0.5*($4-$3)):1:(0.5*($4-$3)):2 w xyerr ti "" lw 2')
	g.close()
	
