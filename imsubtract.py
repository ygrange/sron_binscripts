#!/usr/bin/python

import sys
import pyfits as pf

srcfil=sys.argv[1]
clfil=sys.argv[2]
blfil=sys.argv[3]
fctr=sys.argv[4]
output=sys.argv[5]

source=pf.open(srcfil)
closed=pf.open(clfil)
blank=pf.open(blfil)

A=(closed[0].data) * float(fctr)

source[0].data = source[0].data-(blank[0].data+A)

source[0].header.add_history("removed background, scaled with a factor "+fctr)
source.writeto(output, clobber=True)

source.close()
closed.close()
blank.close()
