#!/usr/bin/python

"""
This script will calculate the factor needed to subtract the background of an image correctly. 
If you only want to subtract a closed filter obs., indicate a "0" at the position of the blank sky file (last pos).
The order of the command line arguments is data, closed filter and blank field observation. All need to be OOFV and must have the same surface.
"""

import sys
import pyfits as pf

def comp(a,b):
	for val in a:
		if val in b:
			return True
	return False

if comp(['h','help','-h','--help'],sys.argv[1:]) or ((len(sys.argv) is not 4)) :
	print __doc__
	sys.exit(0)

dataf=sys.argv[1]
closedf=sys.argv[2]
blankf=sys.argv[3]

data=pf.open(dataf)
datatim=data[1].header['ONTIME07']
datac=len(data[1].data)
data.close()

closed=pf.open(closedf)
closedtim=closed[1].header['ONTIME07']
closedc=len(closed[1].data)
closed.close()

if blankf == "0":
	blanktim=1.
	blankc=0.
else:
	blank=pf.open(blankf)
	blanktim=blank[1].header['ONTIME07']
	blankc=len(blank[1].data)
	blank.close()

datacr=float(datac)/float(datatim)
blankcr=float(blankc)/float(blanktim)
closedcr=float(closedc)/float(closedtim)

	
fctr=(datacr - blankcr)/closedcr

print fctr