#!/usr/bin/python
""" 
Corrects for the out of time events in a spectrum. Give inputfile, ootfile and outfile (in this order) as command line arguments.

"""

import pyfits as pf
import sys
from yan import *


def main():
    try:
        inputfile=sys.argv[1]
        ootfile=sys.argv[2]
        outfile=sys.argv[3]
    except:
	print __doc__
	print "You did not specify three command line arguments, try again"
	sys.exit(1)
    
    spectrum=pf.open(inputfile)
    ootim=pf.open(ootfile)
    
    
    spdata=spectrum[1]
    sphead=spectrum[0].header
    ootdat=ootim[1].data.field('COUNTS')
    
    
    factor={
    	"PrimeFullWindow": lambda : 6.3 ,
	"PrimeFullWindowExtended" : lambda : 2.3 ,
	"PrimeSmallWindow": lambda : 1.1 ,
	"PrimeLargeWindow": lambda : 0.16
	}[sphead['SUBMODE']]()
	
    sf=factor/100.
    
    spdata.data.field('COUNTS')[:]=spdata.data.field('COUNTS') - (ootdat * sf)
    
    #print (spdata.data.field('COUNTS'))[184]
    
    sphead.add_history("OOT subtracted. Using a factor of "+str(factor)+"%")
    
    spectrum.writeto(outfile,clobber=True)
    #spdata.data = spdata.data * scale
    
    #spectrum.writeto(outfile)
   
    spectrum.close()
    ootim.close()

main()
#submode=ff[0].header['SUBMODE']

