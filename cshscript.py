#!/usr/bin/python

"""
This script tries to wrap itself around a csh script to execute it in bash. There is no guarantee whatsoever that it actually works. It should work in the most simple cases.

author: Y. Grange
"""

import sys

if (sys.argv[1]=="h" or sys.argv[1]=="help" or sys.argv[1]=="-h" or sys.argv[1]=="--help"):
	print __doc__
	sys.exit(0)
try:
	fh=open(sys.argv[1])
	lines=fh.readlines()
except:
	print "file error or file not found."
	sys.exit(2)
	
output=list()
if lines[0].__contains__("#!"):
	i=1
else:
	i=0
output.append("#!/bin/bash")
for lin in lines[i:]:
	content=lin.replace('.csh','.sh').split()
	
	if(content[0] is not "#"):
		if(content[0] == "setenv"):
			content[0]="export"
			content[1]=content[1]+"="+content[2]
			content[2]=''
	if content[0].__contains__("tcsh"):
		content=''		

	output.append(' '.join(content))
	
			
			

fn=sys.argv[-1]
out=open(fn,"w")
for lin in output:
	out.write(lin+"\n")
out.close()
