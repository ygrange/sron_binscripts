#!/usr/bin/env perl

################################################################################
#accept a series of images of a certain format and turn
#them into an animation file produced by mencoder (v 0.01)
#requires imagemagick (convert & identify) and mencoder to work
#don't email me: jberk@science.uva.nl
#example
#perl animate.pl --dir /net/astro005/scratch/starlab-4.3/src/node/dyn/util -l 0 -u 0010 -i starlab
#will put all the files with filebase 'starlab' and numbered 0000 through 0010 in that dir
#perl animate.pl --dir /net/astro005/scratch/starlab-4.3/src/node/dyn/util -i starlab -s 0.5 -v 
################################################################################
use strict;
use warnings;

use Data::Dumper;

my $identify = 'identify';
my $convert = 'convert';
my $mencoder = 'mencoder';
my $dirbase = '.';
my @preferred_suffixes = ('.png.tar.gz','.png', '.tga', '.svg', '.jpg',
	 '.fits', '.gif','.ppm', '.pnm', '.rgb');


################################################################################
#nothing to see past here (WARNING completely untested code below! usual non-rights apply)
################################################################################

my @preferred_suffixes_safe;
for (my $i=0; $i <= $#preferred_suffixes; $i++) {
	$preferred_suffixes_safe[$i] = $preferred_suffixes[$i];
	$preferred_suffixes_safe[$i] =~ s/\./\\./g;
}

my $number = qr/[+-]?(?:\d+\.\d+|\d+\.|\.\d+|\d+)(?:[Ee][+-]?\d+)?/;
my $int = qr/[+-]?\d+/;
my $posint = qr/[+]?\d+/;
my $negint = qr/-?\d+/;
my $string = qr/[\w \.]+/;


#set the restrictions which the options given must meet
#if not, the option will be ignored or the default value is used when present
my %cmdrestrictions = (
	'-v'=>'Flag:Def=True',
	'-w'=>'Flag:Def=False',	
	'-b'=>'Int:Def=0',
	'-r'=>'Float:0-100',
	'-d'=>'PInt:[8,16,24,32]',
	'--pres'=>'Flag',	
	'--codec'=>'String:[mjpeg,h263,h263p,mpeg4,msmpeg4,msmpeg4v2,wmv1,rv10,mpeg1video,huffyuv]:Def=msmpeg4v2',	
	'-p'=>'PInt:1-3:Def=2',#more than 3 passes doesn't make any sense
	'-q'=>'PInt:0-120:Def=100',	
	'-f'=>'PInt:Def=24',
	'-o'=>'String:Def=Starlab.avi',	
	'-l'=>'Int:Def=0',
	'-u'=>'String:Def=0',
	'-s'=>'Float'	
	);
	
my %options;
if ($#ARGV > 0) {
	%options = extractoptions(@ARGV);
	applyrestrictions(\%options,\%cmdrestrictions); 
	#print Data::Dumper->new([\%options],[qw(options)])->Indent(1)->Quotekeys(0)->Useqq(1)->Dump;		
	
	if (defined($options{'-m'})) {
		$mencoder = $options{'-m'};
	}
	if (defined($options{'-c'})) {
		$convert = $options{'-c'};
	}	
	if (defined($options{'--dir'})) {
		$dirbase = $options{'--dir'};
	}		
}

#read the $dirbase to check which of the preferred files are present
#the best to suit the criteria will be taken if no other files have been specified
#if no filebase is specified, the bare options are taken to be the filenames
#if no bare options are specified the files found in the $dirbase dir are taken
my @files;
unless (defined($options{'bare options'})) {
	if ($options{'-i'}) {
		if ($options{'-u'} > $options{'-l'}) {
			#use the filebase supplied
			@files = get_these_files_with_filebase($options{'-i'}, $dirbase, $options{'--suff'}, $options{'-l'}, $options{'-u'});
		}
		else {
			@files = get_files_with_filebase($options{'-i'}, $dirbase, $options{'--suff'});
		}
	}
	else { 
		#if no filebase is given, search for files
		@files = find_files_in($dirbase);
	}
}
else {
	#use the bare options as input files
	@files = use_bare_options($options{'bare options'});
}

#test to see if there are images found
unless (@files) {
	print STDERR "no images found!\n";
	exit;
}


#determine the image sizes if some are out of place they will not be taken along
my $pad_to = length("$#files");	
my %images_sizes;
for(my $i=0; $i <= $#files; $i++) {
	my $nr = padintstr($i, $pad_to);
	my ($width, $height) = get_image_size("$dirbase/$files[$i]");

	if(defined($width) && defined($height)) {	
		unless(defined($images_sizes{"$width"."x"."$height"})){
			$images_sizes{"$width"."x"."$height"} = [$files[$i]];
		}
		else {
			push(@{$images_sizes{"$width"."x"."$height"}}, $files[$i]);
		}		
	}	

}


#find the size that is most common
#and select only those images which have this $most_common_size 
my $counter = 0;
my $most_common_size;
foreach my $size (keys %images_sizes) {
	unless ($counter) {
		$most_common_size = $size; 
	}
	else {
		if ($#{@{$images_sizes{$size}}} > $#{@{$images_sizes{$most_common_size}}}) {
			$most_common_size = $size; 			
		}
	}
}

@files = @{$images_sizes{$most_common_size}};

my $width;
my $height;
#if no bitrate has been set calculate optimum
if ($options{'-b'} == 0) {
	$width = 480;
	$height = 320;
	
	if ( $most_common_size =~ /(\d+)x(\d+)/) {
		$width = $1;
		$height = $2;
	}
	
	#W*H*fps*qualityfactor
	$options{'-b'} = $width*$height*$options{'-f'}*$options{'-q'}/512;
}

#what codec?
if (defined($options{'--pres'})){
	if ("$options{'--pres'}" eq "True") {
		$options{'--codec'} = 'msmpeg4v2';
	}
}

my $v4mv="";
if ($options{'--codec'} eq "mpeg4") {
	$v4mv = ":v4mv";
}


#convert all the files to .sgi bitmaps
if ($options{'-v'} eq 'True'){
	print STDERR "converting to intermediate sgi bitmap\n";
}
$pad_to = length("$#files");	
for(my $i=0; $i <= $#files; $i++) {
	my $nr = padintstr($i, $pad_to);
	convert_to_sgi($dirbase, $files[$i], $nr, $options{'-w'});
}

#feed them to mencoder:

#begin building the mencoder cmd str based on the supplied options
my $lavcopts = "vbitrate=$options{'-b'}";
   $lavcopts .= ":mbd=2$v4mv:trell:cbp:mv0";
   $lavcopts .= ":keyint=128:vb_strategy=1:autoaspect";	  
   #see the mencoder manpages for these options
	 

#do the passes
my $currentoutput;
for (my $pass = 1; $pass <= $options{'-p'}; $pass++) {
	unless($pass == $options{'-p'}) {
		$currentoutput = '/dev/null';
	}
	else {
		$currentoutput = $options{'-o'};
		#fix the filename
		if ($currentoutput =~ /^\s*(.+)(\.avi|\.mpeg|\.mpg)\s*$/) {
			$currentoutput = $1;
		}
		if ($options{'--codec'} eq "msmpeg4" ||	$options{'--codec'} eq "msmpeg4v2" ||
			$options{'--codec'} eq "mpeg1video") {
			$currentoutput .= ".mpg";
		}
		else {
			$currentoutput .= '.avi';
		}
	}
	
	my $scale = "";
	if (defined($options{-s})) {
		$scale = "-vf scale=".$width*$options{'-s'}.":".$height*$options{'-s'};
	}
	
	my $mencodercmd = "$mencoder -ovc lavc -lavcopts vcodec=$options{'--codec'}";
	  $mencodercmd .= " -nosound -mf type=sgi:fps=$options{'-f'} $scale -o $currentoutput mf://$dirbase/_temp_file_\\*.sgi";	
	
	if ($options{-v} eq 'True') {
		print STDERR "$mencodercmd\n";		
	}	
	
	system("$mencodercmd");
	unless($pass == $options{'-p'}) {
		system('rm -f frameno.avi');
	}	
	
}
	 
#clean up afterwards
if ($options{'-v'} eq 'True'){
	print STDERR "removing intermediate sgi files\n";
}
for(my $i=0; $i <= $#files; $i++) {
	my $nr = padintstr($i, $pad_to);
	rmsgi($dirbase, $files[$i], $nr);
}

################################################################################
sub get_image_size {
	my ($file) = @_;
	my $cmdstr = "$identify $file |";
	my $identifystr = "";
	open (IDENTIFY, $identify." "."$file |");
	while (<IDENTIFY>){
		$identifystr = $identifystr.$_;
	}
	close(IDENTIFY);
	
	my $width;
	my $height;
	if ($identifystr =~ /$file\s*\w+\s*(\d+)x(\d+)/) {
		$width = $1;
		$height = $2;
	}
	return ($width, $height);
}


sub rmsgi {
	my ($dir, $file, $nr) = @_;
	
	my $sgifile = $dir."/_temp_file_$nr".$file.".sgi";	
	
	my $rmstr = "rm -f $sgifile"; 
	system($rmstr);
	$rmstr = "rm -f divx2pass.log"; 
	system($rmstr);	
}

sub convert_to_sgi {
	my ($dir, $file, $nr, $overwrite) = @_;
	
	my $sgifile = $dir."/_temp_file_$nr".$file.".sgi";	
	$file = $dir."/".$file;
	
	if (-e "$sgifile") {
		if($overwrite eq "False") {
			print STDERR "$sgifile exists\n";	
			return 0;
		}
	}	
	
	my $convertstr = "$convert $file $sgifile"; 
	system($convertstr);
}

sub get_files_with_filebase {
	my ($filebase, $dir, $suffix) = @_;

	my @files;
	
	if (defined($suffix)) {
		opendir(DIR, "$dirbase");
		@files = grep(/.*?$filebase.*?$suffix$/,readdir(DIR));
		close(DIR);
	}
	else {
		foreach my $suffix (@preferred_suffixes) {
			opendir(DIR, "$dirbase");
			my @tempfiles = grep(/.*?$filebase.*?$suffix$/,readdir(DIR));
			
			foreach my $tempfile (@tempfiles) {
				push(@files, $tempfile);
			}
			close(DIR);	
		}
	}

	@files = reverse sort {$b cmp $a} (@files);
	
	return @files;
}

sub get_these_files_with_filebase {
	my ($filebase, $dir, $suffix, $minvalue, $maxvalue) = @_;

	#find padding necessary
	my $pad_to = length("$maxvalue");		
	
	my @files;
	for (my $i = $minvalue; $i <= $maxvalue; $i++) {
		my $nr = padintstr($i, $pad_to);
		my $file = $filebase.$nr;
		
		if (defined($suffix)) {
			my $filecomp = $file.$suffix;
			if (-e "$dir/$filecomp") { 
			 push(@files, $filecomp);			
			}
		}
		else {
			foreach my $suffix (@preferred_suffixes) {
				my $filecomp = $file.$suffix;
				#print "$dir/$filecomp\n";
				#test whether this file with this suffix exists
				if (-e "$dir/$filecomp") { 
					push(@files, $filecomp);				
					last;
				}
			}
		}
		

	}
	
	return @files;
}

sub use_bare_options {
	my ($bare_options) = @_;
	
	my @files = split(/\s+/, $bare_options);
	
	#remove empty strings
	my $splitfiles = $#files;
	for(my $i=0; $i <= $splitfiles; $i++) {
		my $filename = shift (@files);
		if ($filename =~ /^\s*$/) {
			next; 
		}
		else {
			push(@files, $filename);
		}
	}		
	
	
	@files = reverse sort {$b cmp $a} (@files);
	return @files;
}

sub find_files_in {
	my ($dirbase) = @_;

	my @found_files;
	foreach my $suffix (@preferred_suffixes_safe) {
		my $suffixregexp = qr/$suffix/;
		opendir(DIR, "$dirbase");
		my @files = grep(/.+?$suffixregexp$/,readdir(DIR));
		#reduce to the largest group that have the same filebase
	
		my %filebases;
		for (my $i=0; $i <= $#files ;$i++) {
			if ($files[$i] =~ /(.+?)\d+.$suffixregexp/) {
				if (defined($filebases{$1})) {
					$filebases{$1}[0]++;
					push(@{$filebases{$1}[1]},$files[$i]);
				}
				else {
					$filebases{$1} = [1,[$files[$i]]];
				}
			}
		}	
		#find the largest number
		my $maxlength = -1;
		while(my ($key, $arrayref) = each(%filebases)) {
  		  # do something with $key and $value
				my @value = @$arrayref;
				if (length($value[1]) > $maxlength) {
					@found_files = $value[1];
				}
		}
	
		if ($#found_files < $#files) {
			@found_files = @files;
		}
		closedir(DIR);
	}

	@found_files = reverse sort {$b cmp $a} (@found_files);
	#@found_files contains all the filenames of the suffix which has the most of them
	return @found_files;
}
sub applyrestrictions{
	my ($optionsref, $restrictionsref) = @_;

	my %types = (
		'Int' => {
			'typeregexp' => qr/$int/,
			'range' => qr/:\s*($int)\s*-\s*($int)/,
			'choice' => qr/\s*\[\s*((?:$int|,|\s)+)\s*\]\s*/,
			'default' => qr/:\s*Def\s*=\s*($int)/
			},
		'PInt' => {
			'typeregexp' => qr/$posint/,		
			'range' => qr/:\s*($posint)\s*-\s*($posint)/,
			'choice' => qr/\s*\[\s*((?:$posint|,|\s)+)\s*\]\s*/,
			'default' => qr/:\s*Def\s*=\s*($posint)/
			},				
		'NInt' => {
			'typeregexp' => qr/$negint/,		
			'range' => qr/:\s*($negint)\s*-\s*($negint)/,
			'choice' => qr/\s*\[\s*((?:$negint|,|\s)+)\s*\]\s*/,
			'default' => qr/:\s*Def\s*=\s*($negint)/
			},
		'Float' => {
			'typeregexp' => qr/$number/,
			'range' => qr/:\s*($number)\s*-\s*($number)/,
			'choice' => qr/\s*\[\s*((?:$number|,|\s)+)\s*\]\s*/,
			'default' => qr/:\s*Def\s*=\s*($number)/
			},			
		'Flag' => {
			'typeregexp' => qr//,
			'default' => qr/:\s*Def\s*=\s*(True|False)/
			},
		'String' => {
			'typeregexp' => qr/$string/,		
			'choice' => qr/\s*\[\s*((?:$string|,|\s)+)\s*\]\s*/,
			'default' => qr/:\s*Def\s*=\s*($string+)/
			},				
		);
		
	my %shadow_options;

	while ( my ($flag, $restriction) = each(%$restrictionsref) ) {
		while ( my ($type, $typeregexps) = each(%types) ) {#$typeregexps is a hash containing the regexps which are to be tesed with
			if ($restriction =~ /^\s*$type.*$/) {
				#extract the restriction parameters (range, choice, default)
				while ( my ($param, $regexp) = each(%$typeregexps) ) {
					if ($param eq 'typeregexp'){#keep that for later
						$shadow_options{$flag}{$param} = $regexp;				
						next;
					}				
					if (my @extracts = ($restriction =~ /$regexp/)) {
						if ($#extracts != 0){
							$shadow_options{$flag}{$param} = [@extracts]; 
						}
						else {
							#if this is a list, turn into array
							if ($param eq 'choice') {
								$extracts[0] = [split(/,/,$extracts[0])];
							}
							$shadow_options{$flag}{$param} = $extracts[0];							
						}
					}				
				}
				#last;#go to next %$restrictionsref entry
			}
		}					
	}
	#print Data::Dumper->new([\%shadow_options],[qw(shadow_options)])->Indent(1)->Quotekeys(0)->Useqq(1)->Dump;	
	
	#now to actually see if the options in $optionsref follow these restrictions
	#if not, set the default if available, otherwise let it stand
	while ( my ($flag, $restrictionref) = each(%shadow_options) ) {
		#if the option is not given to begin with and no def is supplied: skip
		unless(defined($optionsref->{$flag}) || defined($restrictionref->{'default'})) {
			next;
		}
		unless (defined($optionsref->{$flag})) {;
			$optionsref->{$flag} = $restrictionref->{'default'};		
			next;
		}		

		#first see if the type matches
		#if not, remove from hash
		unless ($optionsref->{$flag} =~ /$restrictionref->{'typeregexp'}/){
			delete $optionsref->{$flag};
		}
		
		#see if in right range
		if (defined($restrictionref->{'range'})) {
			unless($optionsref->{$flag} >= $restrictionref->{'range'}[0] &&
				 $optionsref->{$flag} <= $restrictionref->{'range'}[1]) {
				if (defined($restrictionref->{'default'})) {
					$optionsref->{$flag} = $restrictionref->{'default'};
				}
				else {
					delete $optionsref->{$flag};					
				}
			}
		}
		
		#see if among choices
		if (defined($restrictionref->{'choice'})) {
			foreach my $element (@{$restrictionref->{'choice'}}) {
				if ("$element" eq "$optionsref->{$flag}") {
					$restrictionref->{'default'} = $optionsref->{$flag};
					last;
				}		
			}
			if (defined($restrictionref->{'default'})) {
					$optionsref->{$flag} = $restrictionref->{'default'};
			}
			else {
				delete $optionsref->{$flag};
			}
		}		
	}	
}

sub extractoptions{
	#to cmdoptions raw
	my @cmdargs = @_;
	my $cmdline = '';
	my @cmdsplit;
	
	my %options;
	
	my $flag = qr/\s+?-+[a-zA-Z]\S*/;	
	my $flagspaceless = qr/-+[a-zA-Z]\S*/;		
	
	# make sure there are cmd options to begin with
	if (@cmdargs){
		#flatten the array to a string
		$cmdline = join(' ', @cmdargs);
		#add \s to front and back to make life easier
		$cmdline = " ".$cmdline." ";
		
		#look for anything that could be an option
		@cmdsplit = split(/($flag)/, $cmdline);

		#add trailing and leading spaces to options and their arguments and remove empty ones
		my $elements = $#cmdsplit;
		for(my $i=0; $i <= $elements; $i++) {
			my $element = shift (@cmdsplit);
			if ($element =~ /^\s*(.*?\S+.*?)\s*$/) {#needs testing
				$element = $1; 
			}
			else {
				next;
			}
			$element = " ".$element." ";
			push(@cmdsplit, $element);
		}					

		
		#find all the bare words (that is elements from @cmdsplit that do not fit $flag)
		$options{'bare options'} = "";
		for (my $i=0; $i <= $#cmdsplit; $i++) {
			if (!($cmdsplit[$i] =~ /$flag/)) {
				if($i ==0) {#no leading space
					$options{'bare options'} = $cmdsplit[$i];
				}				
				else {
					$options{'bare options'} = $options{'bare options'}." $cmdsplit[$i]";					
				}
			}
			else{
				last;
			}
		}
		if ($options{'bare options'} eq "") {
			delete $options{'bare options'};
		}
		
		#place options in $optionshashref with arguments without superfluous spaces
		for(my $i=0; $i <= $#cmdsplit; $i++) {
			if ($cmdsplit[$i] =~ /^\s*($flagspaceless)\s*$/) {
				my $key = $1;
				#if there is no argument, this is just a flag
				$options{$key} = "True";												
				if ($#cmdsplit > $i) {
					if (!($cmdsplit[($i+1)] =~ /^\s*($flag)\s*$/)) {
						if ($cmdsplit[($i+1)] =~ /^\s*(.+?)\s*$/) {
							$options{$key} = $1;
						}
					}
				}
			}		
		}
	}
	return %options;
}


#	convert strings to proper nrs
sub str2nr{
	my ($string) = @_;

	my $mantissa = 0;
	my $exponent = 0;
	my $nr = undef;

	#	match [+ | -](int.int | .int int | int.| int)e[+ | -]int, ie common nr format
	if ("$string" =~ /^\s*([+-]?(?:\d+\.\d+|\d+\.|\.\d+|\d+))(?:[Ee]([+-]?\d+))?\s*$/) {
		$mantissa = $1;

		if ($2) {
			$exponent = $2;
		}
	}

	$nr = $mantissa*(10**($exponent));

	return $nr;
}

#	convert strings to proper nrs
sub str2int{
	my ($string) = @_;

	my $mantissa = 0;
	my $exponent = 0;
	my $nr = undef;

	#	match [+ | -](int.int | .int int | int.| int)e[+ | -]int, ie common nr format
	if ("$string" =~ /^\s*([+-]?(?:(\d+)\.\d+|\d+\.|\.\d+|\d+))(?:[Ee]([+-]?\d+))?\s*$/) {
		$nr = $2;

	}

	return $nr;
}



sub padintstr{
	my ($int, $total_length) = @_;
	
	$int = "$int";
	
	my $length = length($int);
	
	my $padding = $total_length - $length;
	
	$int = "0" x $padding.$int;
	
	return $int;

}
