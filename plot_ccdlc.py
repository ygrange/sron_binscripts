#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg") # other possible backends: 
                         # print matplotlib.rcsetup.all_backends
import pylab as pl
import pyfits as pf
import sys
from numpy import array
from os import getcwd
from cal import rgsfactors as factors # ygg-routine.
"""
 Plots the lightcurve of the CCDs. The command line argument is an XMM event list.
"""

fl=sys.argv[1]

if "R1" in fl or "r1" in fl:
	rgs=1
elif "R2" in fl or "r2" in fl:
	rgs=2
else:
	rgs=int(raw_input("Which RGS are we plotting? "))
	
fh=pf.open(fl)
datX=fh[1].data.field('CCDNR')  # second table, the 'CCDNR' column
datY=fh[1].data.field('COUNTS') # second table, the 'COUNTS' column
fh.close()

for i,j in enumerate(datX):
	datY[i]=factors(rgs,j)*float(datY[i])

datX=datX-0.5
errX=array([0.5]*len(datX))
oid=(getcwd()).split("/")[-2]

pl.ion() # If you want interactive plotting (with buttons for save and such)

pl.axhline(linewidth=2, color='r')
pl.errorbar(datX,datY,xerr=errX,fmt="bo")
#pl.plot(datX,[0]*len(datX),"r-")
pl.suptitle("Corrected count rate in the RGS CCDs")
pl.title("obs. id: "+oid)
pl.xlabel("CCRNR")
pl.ylabel("COUNTS")
ax = pl.axes()
#ax.set_xlim(max(0,ot[0]-(nsig+2)*sqrt(ot[0])),ot[0]+(nsig+2)*sqrt(ot[0]))
pl.draw()
pl.savefig('.'.join(fl.split(".")[:-1])+".eps")
