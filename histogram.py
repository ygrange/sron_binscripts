#!/usr/bin/python
import matplotlib
from matplotlib import use as US
#matplotlib.use("Qt4Agg")
US("Qt4Agg")
import pylab as pl
import pyfits as pf
from numpy import where, intersect1d, union1d
from os import  listdir
from sys import argv
from cal import rgsfactors

yeslist=["Y","y","Yes","yes"]

rgsnr=argv[1]
i=int(argv[2])
filename=[a for a in listdir(".") if "merged" in a and "R"+rgsnr in a]
ff=pf.open(filename[0])
CCDNR=ff[1].data.field('CCDNR')
FLAG=ff[1].data.field('FLAG')
PHA=ff[1].data.field('PHA')
#PHA=ff[1].data.field('PI')    
ff.close()

#i=int(raw_input("Which CCDNR do you want to see? "))

idc=where(CCDNR==i)
idf=union1d(where(FLAG==8)[0],where(FLAG==16)[0])
idx=intersect1d(idc[0],idf)
PHA=PHA[idx]
pl.ion()
pl.hist(PHA,1000)
vlag=0
while vlag is 0:
	try:
		xlo=int(raw_input("x lower : "))
	except:
		xlo=0
	try:
		xhi=int(raw_input("x upper : "))
	except:
		xhi=5000
	pl.xlim(xlo,xhi)
	if raw_input("save this? ") in yeslist:
		vlag=1

pl.savefig("R"+str(rgsnr)+"CHIP"+str(i)+".eps")
#if raw_input("Do you want to see the number of counts above minimum value? ") in yeslist:
selPHA=[a for a in PHA if a >= xlo]
ct = len(selPHA)
mini=min(selPHA)
maxi=max(selPHA)
rng=maxi-mini
fac=rgsfactors(int(rgsnr),int(i))
fh=open("R"+str(rgsnr)+"data","a")
fh.write(str(i)+"  "+str(xlo)+"  "+str(xhi)+"  "+str(rng)+"   "+str(ct/fac)+"  "+str(float(ct)/(float(fac)*float(rng)))+" \n")
fh.close()
