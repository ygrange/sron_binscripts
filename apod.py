#!/usr/bin/env python
import sys
import os
prgName = os.path.basename(sys.argv[0])

# mirrors:
apod_mirrors = ["http://antwrp.gsfc.nasa.gov/apod/",
                "http://apod.nasa.gov/apod/",]
movie_extentions = ('mpg', 'mpeg', 'gif', 'avi')
general_error = "Error: unable to download the Astronomy Picture Of the Day for the requested day. The errormessage follows."

# set up usage and help strings
from time import localtime, strftime, mktime, gmtime
g_now = localtime()
g_date_format = "%B %d %Y"
usageStr = "Usage: " + prgName + " [-h | --help] ..options.."
helpStr = usageStr + """

   This utility will download and save pictures that
   are published on Astronomy Picture Of the Day. The way that the
   picture is saved can be controlled by options -b, -f and -p.
   A multitude of mirrors is known to the script.
   If the first one fails the next one is tried.
   The following mirrors are checked in the given order:
   
     """ + "\n     ".join(apod_mirrors) + """
   
   By default the picture of today is downloaded, but other
   pictures can be downloaded as well using the -d option.

   There are also options to automatically set the downloaded
   picture as background on the desktop. This feature only functions
   for the Gnome desktop however.
   
   If the APOD happens to be a movie or an animated gif, the
   first frame is extracted using mplayer when the downloaded
   file is to be set as background. This behaviour
   is triggered for all files with any of the following
   extentions: """ + ", ".join(movie_extentions) + """

   IMPORTANT!!
     Usually the pictures on APOD are copyrighted in some way.
     NEVER use any picture from APOD in your own work
     before checking what rules apply for the particular
     picture you would like to use.

  OPTIONS:
   --set-background
      set the downloaded picture as background
      This option will set the picture as background after
      downloading.
      For now this only works for gnome (using gconftool-2)
   --overwrite-background
      set the downloaded pic as background after deleting the
      current one
      The picture that is currently set as background is removed
      and the newly downloaded picture is saved and set as new
      background.
   -b | --base  directory
      the directory to store the file in
      Default: .
      (ie current dir = dir you started this script in)
   -d | --date date
      the date for which the APOD picture is retrieved.
      The default is to obtain the picture of today. When this
      option is given, the given date is used. Allowed dates
      are any date from June 16 1995 onward, 
      when the first APOD was posted (except June 17, 18, 19
      in 1995; there where no APOD on those dates)
      Formats that can be used to pass the date:
        d
           download day d of the current month, eg -d 10 will 
           get picture of """ + \
           strftime(g_date_format, g_now[:2] + (10,) + g_now[3:]) + """
        d-m or m/d  
           download day d of month m of the current year, eg
           -d 2/8 or -d 8-2 gets picture for """ + \
           strftime(g_date_format, g_now[:1] + (2, 8) + g_now[3:]) + """
        d-m-yyyy or m/d/yyyy
           get picture for day d of month m in year yyyy, eg
           -d 30-7-1998 or -d 7/30/1998 gets the APOD
           of """ + strftime(g_date_format, (1998, 7, 30) + g_now[3:]) + """
        t[oday][-d]
           retrieve picture of d days ago. For example:
           -d t-10 will download the APOD of """ + \
         strftime(g_date_format, gmtime(mktime(g_now[:2] + (g_now[2] - 10,) + g_now[3:]))) + """
        y[esterday]
           retrieve picture of yesterday
   -f | --filename
      name of the file
      Default: original name on the APOD picture
   -p | --path
      full name for the file, including dir
      This option will overwrite options -b and -f
   -q | --quiet
      be quiet
      Normally status messages are printed. All output is
      suppressed with this option allowing the script to
      be run in an automated setup like crontab without
      generating output. Errormessages that cause the script
      to fail are not suppressed though.
   -h | --help
      print this message and quit

Jakob van Bethlehem, Jan 2008
Last update: March 10 2008
   """

# process commandline
import getopt
program_settings = {"base=" : ".",
                    "date=" : "",
                    "filename=" : "",
                    "path=" : "" , "help" : "",
                    "quiet" : False,
                    "set-background" : False,
                    "overwrite-background" : False}
opts, data = getopt.gnu_getopt(sys.argv[1:], "b:d:f:p:hq", program_settings.keys())

for o, v in opts:
  if o in ('-b', '--base'):
    program_settings["base="] = v
    continue
  #
  if o in ('-d', '--date'):
    program_settings["date="] = v
    continue
  #if
  if o in ('-f', '--filename'):
    program_settings["filename="] = v
    continue
  #
  if o in ('-p', "--path"):
    program_settings["path="] = v
    continue
  #
  if o in ('-q', '--quiet'):
    program_settings["quiet"] = True
    continue
  #
  if o in ('-h', '--help'):
    print helpStr
    raise SystemExit
  #
  if o == "--set-background":
    program_settings["set-background"] = True
    continue
  #
  if o == "--overwrite-background":
    program_settings["overwrite-background"] = True
    continue
  #
#
#import commands
import sys
import commands
import re
def set_as_background(filename, overwrite=False):
  """set_as_background(string filename, bool overwrite)

  set the filename as background of the desktop.
  If 'overwrite' is True, the picture that is currently
  set as background is deleted before setting the new
  picture as background. Note that filename must be
  a full path.
  Return true if succesful, false otherwise.

  This works only for gnome sofar.
  """
  # First make sure that the downloaded pic doesn't happen to be a movie:
  # We use the extension for this; not a very save method, but alas:
  ext = re.search(r'(.+)\.([a-zA-Z]+)$', filename)
  if not ext:
    print >> sys.stderr, "Warning: I was not able to find the extention and therefore the type."
    print >> sys.stderr, "         I'll assume it is a normal image file."
  #if
  if ext and ext.group(2) in ('mpg', 'mpeg', 'gif', 'avi'):
    if not program_settings["quiet"]:
      print "\n - Today's picture appears to be a movie.\n - I'll try to extract the first frame for the background"
    #if
    # try to run mplayer to extract the first frame from the movie. This will create a file called
    # '00000001.jpg'
    cmd = "mplayer -vo jpeg:outdir=" + os.path.dirname(filename) + " -frames 1 " + filename
    (st, out) = commands.getstatusoutput(cmd)
    if st:
      print >> sys.stderr, " - Warning: I failed to extract a frame from the movie.\n - I will abort trying to set a background"
      return False
    #if
    # apparently the extraction was succesful. What remains to be done is to:
    # 1: rename 00000001.jpg to a more useful name, which will be the original
    #    filename with extention jpg
    # 2: remove the moviefile
    pic_filename = ext.group(1) + ".jpg"
    os.rename(os.path.dirname(filename) + "/00000001.jpg", pic_filename)
    os.remove(filename)
    print filename, pic_filename
    filename = pic_filename
  #if movie
  # proceed with setting the obtained figure as background. If it 
  # needs to be overwritten, the old pic first needs to be removed
  if overwrite: # get current picture_filename and delete:
    cmd = "gconftool-2 --get /desktop/gnome/background/picture_filename"
    (st, output) = commands.getstatusoutput(cmd)
    try:
      os.remove(output)
    except OSError, e:
      print >> sys.stderr, "Warning: the old background '" + output + "' was not removed. Error follows:"
      print e
    #try
  #if
  # next set the newly obtained picture as background (the following works only for gnome!)
  cmd = "gconftool-2 --type string --set /desktop/gnome/background/picture_filename " + filename
  st = os.system(cmd)
  if st:
    print >> sys.stderr, "Warning: I failed to set today's picture as background."
  #if
  return not st
#set_as_background

## Here the actual script starts
from urlparse import urljoin
import urllib

### Part 1: get the html of the requested page
# first determine the date for which we should download
# the picture. The default is today:
req_day = [g_now[0], g_now[1], g_now[2]]
if program_settings["date="]:
  # overwrite the default day, ie for a usergiven date
  date_match = re.match(r'(\d{1,2})\s*([-/])?\s*(\d{1,2})?\s*([-/])?\s*(\d{4})?', program_settings["date="])
  word_match = re.match(r'(t[oday]{0,4})-(\d+)|(y[estrday]{0,8})$', program_settings["date="])
  if not (date_match or word_match):
    print >> sys.stderr, general_error
    raise SyntaxError, "the requested date " + program_settings["date="] + " could not be parsed. Please check the format and consult the help using -h"
  #if
  
  if date_match:
    # overwrite the default day and/or month:
    req_day[2] = int(date_match.group(1))
    if date_match.group(2) and date_match.group(3):
      # in '-' notation the second number is the month:
      req_day[1] = int(date_match.group(3))
      # if the '/' notation is used, we need to switch the day and month
      if date_match.group(2) == '/':
        tmp = req_day[2]
        req_day[2] = req_day[1]
        req_day[1] = tmp
      #if
    #if
    # finally overwrite the default year as well if requested
    if date_match.group(4) and date_match.group(5):
      if date_match.group(4) != date_match.group(2):
        print >> sys.stderr, general_error
        raise SyntaxError, "the format with '-' and '/' are not allowed to be mixed"
      #if
      req_day[0] = int(date_match.group(5))
    #if
  elif word_match:
    if word_match.group(1):
      if "today".find(word_match.group(1)) == -1:
        print >> sys.stderr, general_error
        raise SyntaxError, "wrong syntax for date " + program_settings["date="] + ". Expected t[oday]-d. Maybe you made a spellingerror?"
      #if
      req_day[2] -= int(word_match.group(2))
    elif word_match.group(3):
      if "yesterday".find(word_match.group(3)) == -1:
        print >> sys.stderr, general_error
        raise SyntaxError, "wrong syntax for date " + program_settings["date="] + ". Expected y[esterday]. Maybe you made a spellingerror?"
      #if
      req_day[2] -= 1
    else:
      print >> sys.stderr, general_error
      raise SyntaxError, "I was unable the requested date " + program_settings["date="] + ". Please check the help with -h"
    #if match
  #if matches?
#if date given?

# First check whether APOD exists for the requested date at all:
# first do a 'normalization' just to be sure:
req_day = tuple([int(n) for n in req_day]) + g_now[3:]
req_day = gmtime(mktime(req_day))
if mktime(req_day) < mktime((1995, 6, 16) + (-1,) * 6):
  print >> sys.stderr, general_error
  raise ValueError, "APOD didn't exist yet at the requested date. See the help"
#if
if req_day[0] == 1995 and req_day[1] == 6 and req_day[2] in (17, 18, 19):
  print >> sys.stderr, general_error
  raise ValueError, "APOD didn't publish a picture on this day. See the help"
#if
if mktime(req_day) > mktime(g_now):
  print >> sys.stderr, general_error
  raise ValueError, "dates in the future are not possible."
#if

# Now we're ready to try and get the picture; first get the html of the requested page
retrieval_oke = False # true if the html was succesfully obtained
match_oke = False     # true if we succesfully filtered out the name of the pic on any mirror
err_string = ""       # error string when retrieval was unsuccesful
picture_match = None  #
# the relative url of the page:
page_url = "ap%2s%2s%2s.html" % (str(req_day[0])[2:].zfill(2), str(req_day[1]).zfill(2), str(req_day[2]).zfill(2))
for mirror in apod_mirrors: # check all mirrors until a succesful retrieval:
  # setup the full url:
  url = mirror + page_url
  
  # open the page
  if not program_settings["quiet"]:
    print "Opening APOD page of " + strftime(g_date_format, req_day),
  #
  try:
    source = urllib.urlopen(url).read()
  except IOError, err_string:
    if not program_settings["quiet"]:
      print "Failed."
    #
    continue
  #try-except
  if not program_settings["quiet"]:
    print "Oke."
  #if
  retrieval_oke = True
  
  # extract the url to the pic. We have to do this inside the loop
  # because the server may return some special page messaging that
  # something is wrong. We can detect this problem not in the
  # foregoing try-statement
  if not program_settings["quiet"]:
    print "Subtracting location of the requested image.",
  #if
  picture_match = re.search(r'<a href="(image/[^"]+)">', source)
  if not picture_match:
    if not program_settings["quiet"]:
      print "Failed."
    #if
    continue
  #if
  if not program_settings["quiet"]:
    print "Oke."
  #if
  
  # if we get to this point the html likely has been read succesfully
  # and we can abort the loop
  match_oke = True
  break
#for

# check for the succesful retrieval. If not succesful, abort the program
if not (retrieval_oke and match_oke):
  print >> sys.stderr, general_error
  raise IOError, "couldn't retrieve the requested page from any of the known mirrors.\n  Retrieval: " + str(retrieval_oke) + " | Match: " + str(match_oke) + " -- Aborting."
#if

### Part 2: download the picture and save it
# determine the full filename to save to:
if program_settings["filename="]:
  filename = program_settings["filename="]
else:
  if program_settings["path="]:
    filename = os.path.join(program_settings["base="], program_settings["path="])
  else:
    filename = os.path.join(program_settings["base="], os.path.basename(picture_match.groups()[0]))
#if-else

# check the existence of the dir where the file is saved to:
if not os.path.exists(os.path.dirname(filename)):
  #make dirs as needed:
  os.makedirs(os.path.dirname(filename))
#if

# try to download:
retrieval_oke = False
err_string = ""
for mirror in apod_mirrors: # check all mirrors until we have a succesful retrieval:
  # setup full url
  full_url = urljoin(mirror, picture_match.groups()[0])

  # try to download the picture
  if not program_settings["quiet"]:
    print "Downloading today's Astronomy Picture Of the Day from " + full_url + ".",
    sys.stdout.flush()
  #if
  try:
    source = urllib.urlretrieve(full_url, filename)
  except IOError, err_string:
    if not program_settings["quiet"]:
      print "Failed."
    #if
    continue
  #try-except

  # if we get to this point the picture must have been downloaded succesfully
  # and we can abort the loop
  retrieval_oke = True
  if not program_settings["quiet"]:
    print "Oke."
  #if
  sys.stdout.flush()  
  break
#for

if not retrieval_oke:
  print >> sys.stderr, general_error
  raise IOError, err_string
#if
if not program_settings["quiet"]:
  print "Succesfully dumped today's picture in " + filename
#

# finally set the file as background if requested:
if program_settings["set-background"] or program_settings["overwrite-background"]:
  if not program_settings["quiet"]:
    print "Setting the downloaded picture as background file of the desktop.",
  #if
  filename = os.path.abspath(filename)
  if set_as_background(filename, program_settings["overwrite-background"]) and not program_settings["quiet"]:
    print "Oke"
  #if
#if