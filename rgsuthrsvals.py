#!/usr/bin/python

from matplotlib import use as US
US("Qt4Agg")
#US("GTKCairo")
import pylab as pl
import pyfits as pf
import sys
from numpy import array, sqrt, where, unique
import os

try:
	SAS_ODF=os.environ["SAS_ODF"]
except:
	SAS_ODF=os.path.abspath("./odf")
	sys.stderr.write("No SAS_ODF environment variable found. Assuming "+SAS_ODF+"\n")

try:
	SAS_RGS=os.environ["SAS_RGS"]
except:
	SAS_RGS=os.path.abspath(SAS_ODF+"/../rgs")
	sys.stderr.write("No SAS_RGS environment variable found. Assuming "+SAS_RGS+"\n")

try:
	arg=sys.argv[1].lower()
except:
	arg="NONE"
	
if arg in ["1","r1","rgs1"]: 
	r="R1"
elif arg in ["2","r2","rgs2"]: 
	r="R2"
else:
	sys.stderr.write("No valid RGS number as input\n")
	sys.exit(2)

workdir=os.getcwd()

os.chdir(SAS_ODF)

auxfile=[a for a in os.listdir(".") if "AUX" in a and r in a][0]
aux=pf.open(auxfile)
da=aux[1].data
cuth=da.field("NUPPERC")
duth=da.field("NUPPERD")
ccdnr=da.field("CCDID")
aux.close()
uth=cuth+duth
ccds=list()
plotdat=list()
for i in unique(ccdnr):
	ids=where(ccdnr==i)
	wuth=uth[ids]
	ccds.append(i)
	plotdat.append(wuth.sum())
	
ccds=array(ccds)
plotdat=array(plotdat)
os.chdir(SAS_RGS)
evtfile=[a for a in os.listdir(".") if "merged" in a and r in a][0]
evt=pf.open(evtfile)
tim=evt[1].header['TELAPSE']
evt.close()

errfrac=1./sqrt(sum(plotdat))
errfracs=1./sqrt(plotdat)
plotdat=plotdat/tim

datX=ccds
errX=array([0.5]*len(datX))
datY=plotdat
oid=(os.getcwd()).split("/")[-2]

pl.ion()

#pl.axhline(linewidth=2, color='r')
pl.errorbar(datX,datY,xerr=errX,fmt="bo")
#pl.plot(datX,[0]*len(datX),"r-")
pl.suptitle("Data over upper threshold for CCDs of RGS "+r)
pl.title("obs. id: "+oid)
pl.xlabel("CCRNR")
pl.ylabel("COUNTS/s")
ax = pl.axes()

#pl.show()

os.chdir(workdir)

pl.savefig(oid+"UPTHLC_"+r+".ps", format="ps")

print oid, plotdat.mean(), errfrac*plotdat.mean()
for i in range(len(plotdat)):
	print oid, "CCD"+str(ccds[i]),plotdat[i], errfracs[i]*plotdat[i]
## Eerst de twee getallen optellen, scheiden per CCDNR en zoeken naar de ontime. Dan plotten.