#!/usr/bin/python

import sys

fin=sys.argv[1]

fh=open(fin)
dat=fh.readlines()
fh.close()

flag=0
for l in dat:
	ll=l.split()	
	if flag is 1:
		parval=l.strip()
		flag=0
	if "W-VAR" in l:
		flag=1
	if "Delta" in l and "Parameter" in l:
		uncert=max(abs(float(ll[5])),abs(float(ll[6])))
	if "band of" in l:
		dof=str(int(ll[1])-1)
	if "WCHI" in l:
		chi=ll[1]
		chired=ll[4]

oid=fin.split("_")[0]

flarval=float(parval)
uncpct=int(abs((uncert/flarval)*100.))

uncpct=str(uncpct)

print oid.ljust(12), parval.ljust(17), uncpct.ljust(12), chi.ljust(5), dof.ljust(3), chired.ljust(5)
		