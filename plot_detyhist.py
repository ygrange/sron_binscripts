#!/usr/bin/python
"""
This script plots a histogram of the cross-dispersion direction (in detector coordinates).The command line input should be at least a RGS number and Chip ID. You can add the word selected if you want plots based on the banana selected event list, havinf the word "selected" in the fits file name. Then, you can also add a spectral order to use (obviously, you also need a selected event list for this order then). 
So, invoking the script with "2 1" as command line parameters would make a plot of RGS2 CCD1. Invoking the script with "2 2 selected 2" will use the file for RGS2 CCD2 order 2 banana selected.
Note that only first and second order are implemented. Any other value will default to "all", which essentially means the script will run as if no order was specified.
"""

import pyfits as pf
import sys
import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
from numpy import where
import numpy as np
import os

if len(sys.argv)<3 or len(sys.argv)>5:
	sys.stderr.write(__doc__)
	sys.exit(2)
	
RI=sys.argv[1]
CI=sys.argv[2]

try:
	sel=sys.argv[3]=="selected"
except:
	sel=False

try:
	OR=sys.argv[4]
	o=abs(int(OR))
	if o>2:
		OR="all"
except:
	OR="all"


if OR!="all" and not sel:
	sel=True
	sys.stderr.write("You can't select an order from the non-selected event list. Actually, what DID you put as a third command line argument anyhow?\n Continuing assuming selected event file and order "+OR+".\n")


if not sel:
	fnam=[a for a in os.listdir(".") if "EVENLI" in a]
	fnam=[a for a in fnam if ".FIT" in a[-4:]]
	fnam=[a for a in fnam if "R"+RI in a][0]
else:
	fnam=[a for a in os.listdir(".") if "selected" in a]
	fnam=[a for a in fnam if RI+"_"+OR in a]
	fnam=[a for a in fnam if ".fits" in a][0]
	
print fnam


fh=pf.open(fnam)
data=fh[1].data.field('CHIPY')
ccdnrs=fh[1].data.field('CCDNR')
flags=fh[1].data.field('FLAG')
ontime=fh[1].header['ONTIME']
fh.close()

upper=float(max(data))+0.5
under=0.5
binsize=1.
bins=[a*binsize+under for a in range(max(data)+1)]

#pl.ion()

idc=where(ccdnrs==int(CI))[0]
idf8=where(flags==8)[0]
idf16=where(flags==16)[0]
idf=np.union1d(idf8,idf16)
ids=np.intersect1d(idf,idc)

data=data[ids]

if len(data)==0:
	sys.exit(2)

c,x,O=pl.hist(data,bins)


x=[x[u-1]+(x[u]-x[u-1])/2. for u in range(1,len(x))] 
c=c/ontime

writedat=np.transpose([x,c])

if OR=="all":
	if not sel:
		pl.savefig("RGS"+RI+"_CCD"+CI+".pdf")
		of=open("RGS"+RI+"_CCD"+CI+".dat","w")
	else:
		pl.savefig("RGS"+RI+"_CCD"+CI+"_selected.pdf")
		of=open("RGS"+RI+"_CCD"+CI+"_selected.dat","w")
else:
	if not sel: # This case should really never happen!
		sys.stderr.write("\n\nATTENTION:\nThis should never happen!!\n\n")
		pl.savefig("RGS"+RI+"_CCD"+CI+"_O"+OR+".pdf")
		of=open("RGS"+RI+"_CCD"+CI+"_O"+OR+".dat","w")
	else:
		pl.savefig("RGS"+RI+"_CCD"+CI+"_O"+OR+"_selected.pdf")
		of=open("RGS"+RI+"_CCD"+CI+"_O"+OR+"_selected.dat","w")

for i in writedat:
	wl="%s  %s \n"%(i[0],i[1])
	of.write(wl)
of.close()

c=c*1e4

pl.clf()
pl.bar(np.array(x)-0.5,c,width=1.)


pl.xlabel("DETY (pixel)")
pl.ylabel(r'Count rate ($10^{-4}\,Cts\,s^{-1}px^{-1}$)')
if OR!="all":
	pl.title("RGS "+RI+" order "+OR+", CCDNR "+CI)
else:
	pl.title("RGS "+RI+", CCDNR "+CI)

#pl.show()
