#!/usr/bin/python


def cts(nr):
    if float(nr)==0.:
	    return [0.,1]
    coefficient = float(nr)
    exponent = 0
    while abs(coefficient) >= 10:
        exponent += 1
        coefficient = coefficient / 10
    while abs(coefficient) < 1:
        exponent -= 1
        coefficient = coefficient * 10
    return [coefficient, exponent]
    
import sys
f=open(sys.argv[1])
raw=f.readlines()
f.close()

rdat=list()
vlag=False
cst=False
chi=False
tv=0.
for i in  raw:
	if ("Paramet" in i) and ("not exist" not in i) and ("coupling" not in i):
		if ("value" not in i):
			rdat.append(i)
			if vlag:
				fields=" ".join(i.split(":")).split()
				sys.stderr.write( "variable: "+" ".join(fields[0:4])+" value: "+tv+"\n\n")
				vlag=False
		else:
			sys.stderr.write( "Lower C-stat warning encountered.\n")
			tv=(i.split())[2]
			vlag=True
	if "method" in i:
		if "method" in i:
			if "C-statistic" in i:
				cst=True
			else:
				chi=True
	if cst:
		if "C-statistic" in i:
			lastlin="C-stat / d.o.f.\t"+i.split(":")[1].strip()+" / "
	if chi:
		if "Chi-squared" in i:
			lastlin="chi^2 / d.o.f.\t"+i.split(":")[1].strip()+" / "
	if chi or cst:
		if "Degrees of freedom" in i:
			lastlin=lastlin+i.split(":")[1].strip()
			chi=False
			cst=False

for line in rdat:
	fields=" ".join(line.split(":")).split()
	val=fields[4]
	pv=" ".join(fields[0:4])
	err=max(abs(float(fields[6])),abs(float(fields[8])))
	valar=cts(val)
	errar=cts(err)
	
	if abs(valar[1]) < 2 or valar[1] == -2:
		valar[0]=valar[0]*10**(valar[1])
		valar[1]=0
		ten_pr=''
	else:
		ten_pr=" * 10^"+str(valar[1])
	
	df=valar[1]-errar[1]
	
	if int(errar[0]) is 1:
		err_pr=str(round(errar[0],1)*10**(-df))
		val_pr=str(round(valar[0],df+1))
	else:
		err_pr=str(round(errar[0])*10**(-df))
		val_pr=str(round(valar[0],df))
		
	print pv," "+val_pr+" \pm "+err_pr+ten_pr
print lastlin
