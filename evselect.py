#!/usr/bin/python

import pyfits    
import math
import os    
import sys
import numpy

def main():    
    feventlist=sys.argv[1]
    fbinmap=sys.argv[2]
    foutpref=sys.argv[3]
    
    # Open Eventfile
    
    eventfile=pyfits.open(feventlist)
    eventlist=eventfile[1].data
    
    # Open binmap
    
    binmap=pyfits.open(fbinmap)
    binimage=binmap[0].data
    
    shapes=binimage.shape
    
#    for i in numarray.arange(shape[0]):
#        for j in numarray.arange(shape[1]):
#	    binimage[i][j]=math.ceil(binimage[i][j])

        
    for i in numpy.arange(int(binimage.max())):
        bin=i+1
	mask = eventlist.field('X') < 0
	for k in numpy.arange(shapes[0]):
	    y=k*80
	    for l in numpy.arange(shapes[1]):
	        x=l*80
		if (math.ceil(binimage[k,l])==bin):
		   mask = mask | (eventlist.field('X') > x) & (eventlist.field('X') < x+80) & (eventlist.field('Y') > y) & (eventlist.field('Y') < y+80)
	
	eventfile[1].data = eventlist[mask]
	eventfile.writeto('events/'+foutpref+'_'+str(bin)+'.fits')
	
	command='source sasinit > /dev/null ; create_spectra.sh '+foutpref+'_'+str(bin)
        os.system(command)

main()
    
