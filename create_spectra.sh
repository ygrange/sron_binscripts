#!/bin/bash

FILE=$1

if [[ $FILE == M1* ]]
then
    echo "Processing $FILE"

    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=11999 spectralbinsize=15 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withimageset=yes xcolumn=DETX ycolumn=DETY \
    imagebinning=binSize ximagebinsize=80 yimagebinsize=80 imageset=events/spectra/${FILE}.ds \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=11999 spectralbinsize=15 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    rm -f events/${FILE}*
fi

if [[ $FILE == M2* ]]
then
    echo "Processing $FILE"
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=11999 spectralbinsize=15 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withimageset=yes xcolumn=DETX ycolumn=DETY \
    imagebinning=binSize ximagebinsize=80 yimagebinsize=80 imageset=events/spectra/${FILE}.ds \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=11999 spectralbinsize=15 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=12)&&(PI in [300:10000]) "
    
    rm -f events/${FILE}*
fi

if [[ $FILE == PN* ]]    
then
    echo "Processing $FILE"
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=20479 spectralbinsize=5 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=4)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withimageset=yes xcolumn=DETX ycolumn=DETY \
    imagebinning=binSize ximagebinsize=80 yimagebinsize=80 imageset=events/spectra/${FILE}.ds \
    expression="(FLAG==0)&&(PATTERN<=4)&&(PI in [300:10000]) "
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=20479 spectralbinsize=5 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=4)&&(PI in [300:10000]) "
    
    rm -f events/${FILE}*
fi

if [[ $FILE == OO* ]]    
then
    echo "Processing $FILE"
    
    evselect table=events/${FILE}.fits withspectrumset=true withspecranges=true \
    energycolumn=PI specchannelmin=0 specchannelmax=20479 spectralbinsize=5 \
    updateexposure=yes writedss=Y spectrumset=events/spectra/${FILE}.pi \
    expression="(FLAG==0)&&(PATTERN<=4)&&(PI in [300:10000]) "
    
    rm -f events/${FILE}*
    
fi
 
