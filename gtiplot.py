#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
from numpy import array
from numpy import append as npap
from numpy import delete as npdel
import sys
import numpy as np
from os import getcwd

oid=getcwd().split("/")[-3]

fl=sys.argv[1]

fh=pf.open(fl)
starts=fh[1].data.field('START')
stops=fh[1].data.field('STOP')
fh.close()

stopy=array([0.]*len(stops))
starty=array([1.]*len(starts))

x=npap(starts,stops)
y=npap(starty,stopy)
x=npap(x,starts-1e-5)
y=npap(y,[0.]*(len(starts)))
x=npap(x,stops-1e-5)
y=npap(y,[1.]*(len(stops)))

ord=x.argsort()

x=x[ord]
y=y[ord]

pl.ion()

pl.plot(x,y,"b-",label="GTI")

ax = pl.axes()
ax.set_ylim(-1.,2.)

pl.savefig("gti_"+oid+".eps")

pl.draw()
raw_input()