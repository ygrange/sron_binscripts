#!/usr/bin/python
"""
This script plots a histogram of the rate of the cross-dispersion direction of two CCD's. The command line input should be a RGS number and both the Chip IDs which you want to divide by eachother, dividing the first by the second. 
So, invoking the script with "2 1 5" as command line parameters would make a plot of RGS2 CCD1 divided by RGS2 CCD5.You need an EVENLI-file in the folder to invoke this script. 
add the keyword "selected" to use source lists in which you mad a selection of the banana.
"""

import pyfits as pf
import sys
import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
from numpy import where, array
import numpy as np
import os

nbins=50         # Number of bins and edges hard coded.
edges=[-2.6,2.6] # Change here if needed


if len(sys.argv)!=4 and len(sys.argv)!=5:
	sys.stderr.write(__doc__)
	sys.exit(2)
	
RI=sys.argv[1]
CI=sys.argv[2]
CI2=sys.argv[3]

try:
	sel=sys.argv[4]=="selected"
except:
	sel=False

binsize=(edges[1]-edges[0])/float(nbins)
bins=[a*binsize+edges[0] for a in range(nbins+1)]


if not sel:
	fnam=[a for a in os.listdir(".") if "EVENLI" in a]
	fnam=[a for a in fnam if ".FIT" in a[-4:]]
	fnam=[a for a in fnam if "R"+RI in a][0]
else:
	fnam=[a for a in os.listdir(".") if "selected" in a]
	fnam=[a for a in fnam if RI+"_all" in a][0]
	
print fnam


fh=pf.open(fnam)
data=fh[1].data.field('XDSP_CORR')
ccdnrs=fh[1].data.field('CCDNR')
flags=fh[1].data.field('FLAG')
fh.close()

data2=data*3600
data=data*3600

pl.ion()


idc=where(ccdnrs==int(CI))[0]
idf8=where(flags==8)[0]
idf16=where(flags==16)[0]
idf=np.union1d(idf8,idf16)
ids=np.intersect1d(idf,idc)

idc=where(ccdnrs==int(CI2))[0]
ids2=np.intersect1d(idf,idc)

#print ids

data=array(data[ids])
data2=array(data2[ids2])

if len(data)==0 or len(data2)==0:
	sys.exit(2)

c,x,O=pl.hist(data,bins)
c2,x2,O2=pl.hist(data2,bins)

c=array(c,dtype=float)
c2=array(c2,dtype=float)

widths=x[1:]-x[:-1]
edges=x[:-1]

crat=c/c2
crat=np.nan_to_num(crat)

for i in range(len(crat)):
	if crat[i]>1e250:
		crat[i]=-0.05

x=[x[u-1]+(x[u]-x[u-1])/2. for u in range(1,len(x))] 
#print len(c), len(x)
writedat=np.transpose([x,crat])

pl.clf()

#print crat
pl.bar(edges,crat, width=widths)
pl.xlabel("XDSP_CORR (')")
pl.ylabel("Number of events ("+str(nbins)+" bins)")
pl.title("RGS "+RI+", ratio of CCD"+CI+" and CCD"+CI2)

#pl.show()
if not sel:
	pl.savefig("RGS"+RI+"_CCD_"+CI+"_to_"+CI2+".pdf")
	of=open("RGS"+RI+"_CCD_"+CI+"_to_"+CI2+".dat","w")
else:
	pl.savefig("RGS"+RI+"_CCD_"+CI+"_to_"+CI2+"_selected.pdf")
	of=open("RGS"+RI+"_CCD_"+CI+"_to_"+CI2+"_selected.dat","w")

for i in writedat:
	wl="%s  %s \n"%(i[0],i[1])
	of.write(wl)
of.close()