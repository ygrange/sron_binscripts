#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
from numpy import array
from numpy import append as npap
from numpy import delete as npdel
import sys
import numpy as np
from os import getcwd

oid=getcwd().split("/")[-3]

fl=sys.argv[1]

fh=pf.open(fl)
indexes=list()

for set in fh:
	if "GTI" in set.name:
		print fh.index_of(set.name), set.name, set.size()
		indexes.append(fh.index_of(set.name))
		
id=raw_input("please input which GTI to plot. If you want all at the same time, type 'all'. > ")
if id == "all":
	addup=True
else:
	addup=False
	try:
		id=int(id)
	except:
		sys.stderr.write("That's not an integer number or 'all'. Please try again.\n")
		sys.exit(2)
if addup:
	starts=list()
	stops=list()
	for id in indexes:
		starts.append(fh[id].data.field('START').tolist())
		stops.append(fh[id].data.field('STOP').tolist())
	
	starts=array(sum(starts,[]))
	stops=array(sum(stops,[]))

starts=fh[id].data.field('START')
stops=fh[id].data.field('STOP')
fh.close()

stopy=array([0.]*len(stops))
starty=array([1.]*len(starts))

x=npap(starts,stops)
y=npap(starty,stopy)
x=npap(x,starts-1e-5)
y=npap(y,[0.]*(len(starts)))
x=npap(x,stops-1e-5)
y=npap(y,[1.]*(len(stops)))

ord=x.argsort()

x=x[ord]
y=y[ord]

pl.ion()

pl.plot(x,y,"b-",label="GTI")

ax = pl.axes()
ax.set_ylim(-1.,2.)

pl.savefig("gti_"+oid+".eps")

pl.draw()
raw_input()