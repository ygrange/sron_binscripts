#!/usr/bin/python

"""
This script will subtract the background of an image correctly. 
The order of the command line arguments is data, closed filter obs, blank field obs, the scale factor from the oofv region and the output file.
"""

import sys
import pyfits as pf

def comp(a,b):
	for val in a:
		if val in b:
			return True
	return False


if comp(['h','help','-h','--help'],sys.argv[1:]) or (len(sys.argv) is 1) :
	print __doc__
	sys.exit(0)
	
srcf=sys.argv[1]
closedf=sys.argv[2]
blankf=sys.argv[3]
fctr=float(sys.argv[4])
outfile=sys.argv[5]

src=pf.open(srcf)
srctim=src[0].header['ONTIME07']
srcim=src[0]


closed=pf.open(closedf)
closedtim=closed[0].header['ONTIME07']
closedim=closed[0]


blank=pf.open(blankf)
blanktim=blank[0].header['ONTIME07']
blankim=blank[0]


blank_sf=srctim/blanktim
closed_sf=srctim/closedtim
closed_sf=closed_sf*fctr	


closedim.scale('float32','',bscale=(1./closed_sf),bzero=0)
blankim.scale('float32','',bscale=(1./blank_sf),bzero=0)

srcim.data=srcim.data-closedim.data-blankim.data
	
srcim.header.add_history("Background subtracted. Using a factor of "+str(closed_sf)+" for closed and "+str(blank_sf)+" for blank")
	
src.writeto(outfile,clobber=True)

src.close()
closed.close()
blank.close()
