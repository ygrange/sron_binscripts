#!/usr/bin/python
"""
example: 
Program to add up vprof files. Equal binning of input files is assumed (which should be met when using the Tamurascript) The command line options are:
"input=[filename]" - the name of a txt file with the names of all vprof files to add. Furthermore a weighting can be added. 
The file should contain the filename followed by an optional weighting factor. If no wieghting is specified, the files are assumed to be equal in weight (i.e. the weight is 1./(number of files). If you indicate less weighting factors than files, the script will crash.
The weights don't have to add up to 1, so you can e.g. use exposure times. The script will take care of the math.
"output=[filename]" - the output file (no extension assumed). 
"""


import sys

version="0.1"

if len(sys.argv)==1:
	print "addvprofs, version "+version
	print "For help, type "+sys.argv[0]+" -h"
	sys.exit(1)

settings=dict()
for a in sys.argv[1:]:
	if a == "-h" or a == "--help":
		print  __doc__
		sys.exit(0)
	else:
		settings[a.split("=")[0]]=a.split("=")[1]

try:
	input=settings["input"]
except:
	input=raw_input("Input file (For help, type "+sys.argv[0]+" -h")
try:
	output=settings["output"]
except:
        output=raw_input("Input file name")

print "\n"

try:
	fi=open(input)
except:
	sys.stderr.write("File error. Please specify an existing file.\n")

indat=fi.readlines()
fi.close()

num=len(indat[0].split())

if (num is not 1) and (num is not 2):
	sys.stderr.write( "The number of parameters on the first line of the listing file seems not to be one or two. \nPlease correct or type "+sys.argv[0]+" -h for help. \n")
	sys.exit(2)

wflag=(num is 2)

	
files=list()
weights=list()

for d in indat:
	if num is not len(d.split()):
		sys.stderr.write("It seems that different lines have different numbers of components. I don't know how to handle this. \nPlease correct or type "+sys.argv[0]+" -h for help. \n")
		sys.exit(2)
	files.append(d.split()[0])
	if wflag: weights.append(float(d.split()[1]))

if not wflag:
	for i in range(len(files)):
		weights.append(1./len(files))

wtot=sum(weights)

for i in range(len(weights)):
	weights[i]=weights[i]/wtot


dd=list()

for i in range(len(files)):
	try:
		ff=open(files[i])
		dd.append(ff.readlines())
		ff.close()
	except:
		sys.stderr.write("file"+files[i]+" could not be opened! bailing out. \n")
		sys.exit(2)

outp=list()

for j in range(len(dd[0])):
	tv=0
	for i in range(len(dd)):
		tv=tv+weights[i]*float((dd[i][j].split())[1])
	outp.append(str((dd[0][j].split())[0])+"\t"+str(tv)+"\n")

of=open(output,"w")
of.writelines(outp)
of.close()

		
