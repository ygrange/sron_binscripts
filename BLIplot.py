#!/usr/bin/python

import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import pyfits as pf
from numpy import array
import sys
import numpy as np

fn=sys.argv[1]

fle=pf.open(fn)
ydat=fle["FACTORS"].data.field('Factors')
xdat=np.arange(1.,len(ydat)+1.)
fle.close()
	
lefts=xdat-0.5
pl.ion()

pl.xlim(0.5,16.5)
pl.xticks(xdat)

pl.bar(lefts,ydat,1,color="w",edgecolor="k")
pl.plot(xdat,ydat,"ko")
pl.xlabel("BLI bin number")
pl.ylabel("Scale factor")

pl.savefig(fn.replace(".FIT",".pdf"))
pl.savefig(fn.replace(".FIT",".eps"))

