#!/usr/bin/python
""" 
Corrects for the out of time events in an image. Give inputfile, ootfile and outfile (in this order) as command line arguments.

"""

import pyfits as pf
import sys

def main():
    try:
        inputfile=sys.argv[1]
        ootfile=sys.argv[2]
        outfile=sys.argv[3]
    except:
	print __doc__
	print "You did not specify three command line arguments, try again"
	sys.exit(1)
    
    image=pf.open(inputfile)
    ootim=pf.open(ootfile)
    
    
    imdata=image[0]
    ootdat=ootim[0]
    
    factor={
    	"PrimeFullWindow": lambda : 6.3 ,
	"PrimeFullWindowExtended" : lambda : 2.3 ,
	"PrimeSmallWindow": lambda : 1.1 ,
	"PrimeLargeWindow": lambda : 0.16
	}[imdata.header['SUBMODE']]()
	
    sf=factor/100.
    imdata.data=imdata.data - (ootdat.data * sf)
    
    imdata.header.add_history("OOT subtracted. Using a factor of "+str(factor)+"%")
    
    image.writeto(outfile,clobber=True)
    #imdata.data = imdata.data * scale
    
    #image.writeto(outfile)
   
    #image.close()

main()
#submode=ff[0].header['SUBMODE']

