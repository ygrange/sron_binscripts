#!/usr/bin/python

"""
CIE version!
script to make tex table the inner circle. This script loops over all parameters on the command line and looks up their real name according to the file named by variable [dicname] and makes a latex table called [outfile] of the parameters. The WDEM cutoff factor is taken according to the [cut] parameter. The file read in has the form of [prefix]7[postfix]. The spex command file should conatain the line "this is error on {par}", where {par} is the name of the parameter, just before error calculation. This will generate an error in the output file which is used by the script to grab the parameter and error values. In stead of a prefix-postfix, one can also specify a completely different file name, with the parameter [filename]. Obviously the pre- and postfix variables will then be ignored.


Default variable values:
   dicname="dictionary.yan"
   prefix="annul_"
   postfix=".out"

To change these parameters, just add a file called "cietexrc" or "texrc" to the folder of your data set. If "cietexrc" exists, "texrc" is ignored! All options can also be fed as command line parameters.

Dependencies:
	Pyclus


"""

import sys

variables=dict()
pubpars=list()

for clv in sys.argv[1:]:
	if(clv=="h" or clv=="help" or clv=="-h" or clv =="--help"):
		print __doc__
		sys.exit(0)
	try:
		variables[clv.split("=")[0].strip().strip('"')]=clv.split("=")[1].strip().strip('"')
	except:
		pubpars.append(clv)

try:
	f=open("cietexrc")
	for entry in (f.readlines()):
		print entry.split("=")[0].strip()
		variables[entry.split("=")[0].strip().strip('"')]=entry.split("=")[1].strip().strip('"')
	f.close()
except:
	try:
		f=open("texrc")
		for entry in (f.readlines()):
			variables[entry.split("=")[0].strip().strip('"')]=entry.split("=")[1].strip().strip('"')
		f.close()
	except:
		sys.stderr.write("no cietexrc or texrc found. All variables will be used as default (or command line).\n")	

try:
	dicname=variables["dicname"]
except:
	dicname="dictionary.yan"
try:
	filename=variables["filename"]
except:
	try:
		prefix=variables["prefix"]
	except:
		prefix="annul_"
	try:
		postfix=variables["postfix"]
	except:
		postfix=".out"
	filename=prefix+"7"+postfix

from pyclus import *

names=dict()
dicto=open(dicname)

for entry in (dicto.readlines()):
	names[entry.split(",")[0]]=entry.split(",")[1].strip()


fh=open(filename)
data=fh.readlines()
fh.close()
dataholder=dict()
errorholder=dict()

flag2=flag=0

for line in data:
	#print dataholder
	if "Input error:" in line:
		flag=1
		unit=line[30:].strip()
		dataholder[unit]=list()
		errorholder[unit]=list()
		dataholder[unit].append(names[unit])
	elif "Chi-squared value " in line:
		lis=line.replace("\n","").split(":")
		dataholder["chisq"]=list()
		dataholder["chisq"].append(names["chisq"])
		dataholder["chisq"].append(str(lis[1]))
	elif "C-statistic" in line:
		lis=line.replace("\n","").split(":")
		dataholder["cash"]=list()
		dataholder["cash"].append(names["cash"])
		dataholder["cash"].append(str(lis[1]))
	elif "Degrees of freedom" in line:
		lis=line.replace("\n","").split(":")
		dof=lis[1]
	elif flag2==1 :
		lis=line.split()
		dataholder["f"]=list()
		dataholder["f"].append(names["f"])
		dataholder["f"].append(str(lis[4]))
		flag2=0
	elif "photons/s" in line:
		flag2=1
		
	if flag==2 :
		if "Parameter" in line and "," in line:
			lis=line.replace(",",":").replace("\n","").replace("Errors","").split(":")
			dataholder[unit].append(str(lis[1]))
			errorholder[unit].append(max(abs(float(lis[2])),abs(float(lis[3]))))
	elif flag==1:
		flag=2

params=list()
values=list()
errors=list()

for par in pubpars:
	#print par
	if(par=="cash" or par=="chisq"):
		statname=dataholder[par][0]
		statval=dataholder[par][1]
	elif(par=="f" or par=="flux"):
		params.append(dataholder[par][0])
		values.append(str(dataholder[par][1]))
		ern=errorholder["n"][0]
		valn=float(dataholder["n"][1])
		errors.append(str((ern/valn)*(float(dataholder[par][1]))))
	else:
		try:
			1/(float(par)-26.)
			if(float(dataholder["26"])is not 0.):
				params.append(dataholder[par][0])
				values.append(str(float(dataholder[par][1])/float(dataholder["26"][1])))
				errors.append(errprop(dataholder[par][1],dataholder["26"][1],errorholder[par][0],errorholder["26"][0]))
			else:
				params.append(dataholder[par][0])
				values.append(str(dataholder[par][1]))
				errors.append(str(errorholder[par][0]))
		except:
			#print "except"
			try:	
				params.append(dataholder[par][0])
				values.append(str(dataholder[par][1]))
				errors.append(str(errorholder[par][0]))
			except:
				sys.stderr.write("Unknown parameter "+par+"\n")
				sys.exit(2)


print "\\documentclass{article}"
print "\\begin{document}"
print "\\begin{table}"
print "\\begin{tabular}{|llll|}"
print "\hline"
print "\\textbf{Parameter} & \\textbf{value} &  & \\textbf{1} $\\mathbf{\sigma}$ \\textbf{error} \\\\"
print "\hline"
print "\hline"
for i in range(len(params)):
	print params[i]+" & "+valformat(values[i])+" & $\pm$ & "+valformat(errors[i])+"\\\\"
try:
	print str(statname)+" / d.o.f."+" & "+str(statval)+" / "+str(dof)+"& &\\\\"
except:
	print str(dataholder["chisq"][0])+" / d.o.f."+" & "+str(dataholder["chisq"][1])+" / "+str(dof)+"& &\\\\"
print "\\hline"
print "\\end{tabular}"
print "\\end{table}"
print "\\end{document}"
