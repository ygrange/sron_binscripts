#!/usr/bin/python
import pyfits as pf
import matplotlib
matplotlib.use("Qt4Agg")
import pylab as pl
import sys
import matplotlib.cm as cm
from numpy import log10, arange,array
import numpy as NP

colmaps={1:cm.jet,2:cm.hot,3:cm.cool,4:cm.hsv,5:cm.spectral,6:cm.binary}
colmnames={1:"jet",2:"hot",3:"cool",4:"hsv",5:"spectral",6:"binary"}

def tostr(ary): 
	print ary[0][0],
	for aa in ary[0][1:]:
		print ",",aa,
	for a in ary[1:]:
		for b in a:
			print ",",b, 
def unique_pairs(arr):
	# Return array with only unique combinations
	retarr=list()
	for a in arr:
		a=list(a)
		if a not in retarr:
			retarr.append(a)
	return retarr
		





def unify(arr):
	inc=[a for a in arr if a[0][0]!="!"]
	exc=[a for a in arr if a[0][0]=="!"]

	incx=list()
	incy=list()
	[incx.append(ii[1]) for ii in inc]
	[incy.append(ii[2]) for ii in inc]
	
	incx=NP.array(incx).flatten()
	incy=NP.array(incy).flatten()	
	incxs=list()
	incys=list()
	for ii,xx in enumerate(incx):
		if (xx>0) and (incy[ii]>0):
			incxs.append(xx)
			incys.append(incy[ii])
	pairs=NP.array([incxs,incys]).transpose()
	pairs=unique_pairs(pairs)
	inc=NP.array(pairs).transpose()
	incx=inc[0]
	incy=inc[1]
	outxl=[list(),list()]
	outyl=[list(),list()]
	be=False	
	prevx=-999

	incix=NP.argsort(incx)
	incx=incx[incix]
	incy=incy[incix]
	for ii,x in enumerate(incx):
		if x == prevx:
			outxl[0].append(x)
			outxl[1].append(x)
			yy=incy[ii]
			outyl[0].append(max(yy,prevy))
			outyl[1].append(min(yy,prevy))
		prevx=x
		prevy=incy[ii]
	
	#outix=NP.argsort(outxl[0])
	outx=array(outxl[0])
	outy=array(outyl[0])
	outx=NP.append(outx,array(outxl[1])[::-1])
	outy=NP.append(outy,array(outyl[1])[::-1])
	outx=outx.flatten()
	outy=outy.flatten()
	
	excx=list()
	excy=list()
	for ee in exc:
		xes=[a for a in ee[1] if a>0]
		ys=[a for a in ee[2] if a>0]
		ix=NP.argsort(xes)
		xes=NP.array(xes)[ix]
		ys=NP.array(ys)[ix]
		excxl=[list(),list()]
		excyl=[list(),list()]
		for ii,x in enumerate(xes):
			if x == prevx:
				excxl[0].append(x)
				excxl[1].append(x)
				yy=ys[ii]
				excyl[0].append(max(yy,prevy))
				excyl[1].append(min(yy,prevy))
			prevx=x
			prevy=ys[ii]
		excxs=array(excxl[0])
		excys=array(excyl[0])
		excxs=NP.append(excxs,array(excxl[1])[::-1])
		excys=NP.append(excys,array(excyl[1])[::-1])
		excxs=excxs.flatten()
		excys=excys.flatten()
		excx.append(excxs)
		excy.append(excys)
	return outx,outy,excx,excy

def unify_sp(arr): # Due to previous consistency, this function messes around with x and y coordinate
                   # DIRTY HACK ALERT!!!!!!!!!!! Realise when editing!!!
	inc=[a for a in arr if a[0][0]!="!"]
	exc=[a for a in arr if a[0][0]=="!"]

	incx=list()
	incy=list()
	[incx.append(ii[2]) for ii in inc]
	[incy.append(ii[1]) for ii in inc]
	
	incx=NP.array(incx).flatten()
	incy=NP.array(incy).flatten()	
	incxs=list()
	incys=list()
	for ii,xx in enumerate(incx):
		if (abs(xx)>0) and (abs(incy[ii])>0):
			incxs.append(xx)
			incys.append(incy[ii])
	pairs=NP.array([incxs,incys]).transpose()
	pairs=unique_pairs(pairs)
	inc=NP.array(pairs).transpose()
	incx=inc[0]
	incy=inc[1]
	outxl=[list(),list()]
	outyl=[list(),list()]
	be=False	
	for ii,x in enumerate(incx):
		if x > 0:
			yy=incy[ii]
			outxl[0].append(x)
			outyl[0].append(yy)
		else:
			yy=incy[ii]
			outxl[1].append(x)
			outyl[1].append(yy)
	
	outix=NP.argsort(outyl[0])
	outx=array(outxl[0])[outix]
	outy=array(outyl[0])[outix]
	outix=NP.argsort(outyl[1])[::-1]
	outx=NP.append(outx,array(outxl[1])[outix])
	outy=NP.append(outy,array(outyl[1])[outix])
	outx=outx.flatten()
	outy=outy.flatten()

	return outx,outy




fnam=raw_input("Input file name: ")
try:
	im=pf.open(fnam)
except:
	print "Something went wrong opening the file"
	sys.exit(2)

if len(im)>1:
	tn=raw_input("Input table number (1 is first): ")

	try:
		if int(tn)>len(im):
			print "You ask for a non-existant table id"
			sys.exit(2)
	except:
			print "You ask for a non-existant table id"
			sys.exit(2)
else:
	tn=1
tn=tn-1

plotdat=im[tn].data
xzeropix=im[tn].header['CRPIX1']
xzeroval=im[tn].header['CRVAL1']
xzerodlt=im[tn].header['CDELT1']
yzeropix=im[tn].header['CRPIX2']
yzeroval=im[tn].header['CRVAL2']
yzerodlt=im[tn].header['CDELT2']
filetype=im[tn].header['CTYPE2']
im.close()

for i in colmaps.keys():
	print i,colmnames[i]

icm=raw_input("Input colour map (1..6): ")

try:
	cm=colmaps[int(icm)]
except:
	print "No legal number inputed."
	sys.exit(2)

plotdat=plotdat[::-1]

logg=False
if raw_input("For log colour scale, type \"log\": ").lower() == "log":
	plotdat=log10(plotdat)
	logg=True
	
maxv=max(plotdat.flatten())



pl.ion()

xw=len(plotdat)
yw=len(plotdat[0])

xtpos=range(0,xw,int(round(xw/5.)))
xtpos.append(xw-1)

ytpos=range(0,yw,int(round(yw/4.)))
ytpos.append(yw-1)

ytpos=array(ytpos)
ylabs=yzeroval+(ytpos+yzeropix)*yzerodlt

xtpos=array(xtpos)
xlabs=xzeroval+(xtpos+xzeropix)*xzerodlt

ylabs=ylabs[::-1]

xlabs=["%.3f"%xx for xx in xlabs]

if filetype=="PI":
	px="1"
elif filetype=="XDSP_CORR":
	px="2"
else:
	sys.stderr.write("File type not recognised")
	px=raw_input("PI (1) or XDSP (2) plot? ")

pl.xlabel("Dispersion angle (rad)")
if px=="1":
	ylabs=["%.0f"%yy for yy in ylabs]
	pl.ylabel("Energy (ev)")
elif px=="2":
	ylabs=["%.5f"%yy for yy in ylabs]
	pl.ylabel("Cross-dispersion angle (rad)")
else:
	print "unknown number"
	sys.exit(2)

pl.xticks(xtpos,xlabs)
pl.yticks(ytpos,ylabs)

pl.imshow(plotdat,cmap=cm)


pp=pl.colorbar()
if logg:
	stepsi=(maxv-.01)/10
	tpos=arange(0.01,maxv,stepsi)
	tpos=tpos.tolist()
	tpos.append(maxv)
	tpos=array(tpos)
	
	pp.set_ticks(tpos)
	tis=["%.2f"%ii for ii in 10**tpos]
	pp.set_ticklabels(tis)
	pp.set_label("Counts")
	
regs=raw_input("Region file (leave empty if none) ")

if len(regs)>0 and px=="1":
	listof=dict()
	fil=pf.open(regs)
	for tab in fil:
		if "SRC" in tab.name and "ORDER" in tab.name:
			listof[int(tab.name.split("_")[-1])]=fil.index_of(tab.name)
	incxs=list()
	incys=list()
	excxs=list()
	excys=list()
	for tbid in listof.values():
		xcrds,ycrds,xexc,yexc=unify(fil[tbid].data)
		incxs.append(xcrds)
		incys.append(ycrds)
		excxs.append(xexc)
		excys.append(yexc)
	
	col='k'
	ls='solid'
	for ii,xx in enumerate(incxs):
		xx=NP.array(xx)
		yy=NP.array(incys[ii])
		px=((xx-xzeroval)/xzerodlt)+xzeropix
		py=((yy-yzeroval)/yzerodlt)+yzeropix
		py=pl.ylim()[0]-py
		pl.fill(px,py,'-',fill=False,linewidth=3,edgecolor=col,linestyle=ls)

	col='r'
	ls='dashed'
	for ii,xxx in enumerate(excxs):
		yyy=excys[ii]
		for jj,xx in enumerate(xxx):
			xx=NP.array(xx)
			yy=NP.array(yyy[jj])
			px=((xx-xzeroval)/xzerodlt)+xzeropix
			py=((yy-yzeroval)/yzerodlt)+yzeropix
			py=pl.ylim()[0]-py
			pl.fill(px,py,'-',fill=False,linewidth=3,edgecolor=col,linestyle=ls)
		#pl.plot(px,py,"*")
		#raw_input()

if len(regs)>0 and px=="2":
	listof=dict()
	fil=pf.open(regs)
	for tab in fil:
		if "SRC" in tab.name and "SPATIAL" in tab.name:
			listof[0]=fil.index_of(tab.name)
		incxs=list()
	incys=list()
	excxs=list()
	excys=list()
	for tbid in listof.values():
		xcrds,ycrds=unify_sp(fil[tbid].data)
		#print xcrds
		incys.append(xcrds)
		incxs.append(ycrds) # due to nature of script, better to swap
				    # (DIRTY HACK ALERT)
	
	col='k'
	ls='solid'
	for ii,xx in enumerate(incxs):
		xx=NP.array(xx)
		yy=NP.array(incys[ii])
		px=((xx-xzeroval)/xzerodlt)+xzeropix
		py=((yy-yzeroval)/yzerodlt)+yzeropix
		py=pl.ylim()[0]-py
		pl.fill(px,py,'-',fill=False,linewidth=3,edgecolor=col,linestyle=ls)


p1=pl.gca()
p2=pl.twiny()
p2.set_xlim(p1.get_xlim())
p2.set_label("CCD number")
ids=NP.array([1,2,3,4,5,6,7,8,9])
pos=NP.array([0.073,0.069,0.065,0.062,0.0575,0.052,0.048,0.0438,0.0395])
ppos=((pos-xzeroval)/xzerodlt)-xzeropix
p2.set_xticks(ppos)
p2.set_xticklabels(ids)

fno=raw_input("Output file name ")
if fno[-4:]!=".eps":
	fno+=".eps"
pl.savefig(fno)

	