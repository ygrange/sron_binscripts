#!/bin/bash
# only change the APOD dir.
# version: 12-08-2006


#nothing to see below here...

APOD_DIR=/home/$USER/apod
prefix=${HOSTNAME}

test=$(ps -eu $USER | grep sshd | wc -l)
#test=$( /usr/bin/w | /bin/awk  ' /'$USER'/  {print " " $3} ' | /bin/fgrep ' -'| /usr/bin/wc -l )
#if [ "$test" != "0" ] && [ "$HOST" != "mremote" ] 
if [ "$test" == "0" ]
then

if [ "$1" = "" ]; then
    date=$(date -d'5 hours ago' +'%y%m%d')
else
    date=$1
fi
test2=$(ls $APOD_DIR | fgrep "${prefix}_apod_$date." | wc -l)
if [  "$test2" == "0"  ]
then
sleep 20

rm $APOD_DIR/${prefix}_apod_*

ext=$(curl --silent http://apod.nasa.gov/apod/ap$date.html | awk '/[Ii][Mm][Gg] [Ss][Rr][Cc]/' | sed -e 's/<IMG SRC=\"/http:\/\/apod.nasa.gov\/apod\//g' | awk ' {split($0,arr,"\"") ; print $1}' | awk ' {print(substr ($1,length($1)-3,3)) }')

touch $APOD_DIR/${prefix}_apod_$date.
while [[ -z $ext ]]
do

date=$(date -d "$date - 1 day" +'%y%m%d')

ext=$(curl --silent http://apod.nasa.gov/apod/ap$date.html | awk '/[Ii][Mm][Gg] [Ss][Rr][Cc]/' | sed -e 's/<IMG SRC=\"/http:\/\/apod.nasa.gov\/apod\//g' | awk ' {split($0,arr,"\"") ; print $1}' | awk ' {print(substr ($1,length($1)-3,3)) }')
echo using $date in stead of today
done

# ext=$(curl --silent http://antwrp.gsfc.nasa.gov/apod/ap$date.html | awk  '/[Ii][Mm][Gg] [Ss][Rr][Cc]/'| sed -e 's/<IMG SRC=\"/http:\/\/antwrp.gsfc.nasa.gov\/apod\//g' | awk ' {print(substr ($0,length($0)-3 ,3 ))}')

src=$(curl --silent http://apod.nasa.gov/apod/ap$date.html | awk '/[Ii][Mm][Gg] [Ss][Rr][Cc]/' | sed -e 's/<IMG SRC=\"/http:\/\/apod.nasa.gov\/apod\//g' | awk ' {split($0,arr,"\"") ; print $1}' | awk ' {print(substr ($1,1,length($1)-5))}' )

#src=$(curl --silent http://antwrp.gsfc.nasa.gov/apod/ap$date.html | awk  '/[Ii][Mm][Gg] [Ss][Rr][Cc]/'| sed -e 's/<IMG SRC=\"/http:\/\/antwrp.gsfc.nasa.gov\/apod\//g' | awk ' {print(substr ($0,1,length($0)-5))}')

img=$(echo $src.$ext)

dest=$APOD_DIR/${prefix}_apod_$date.$ext

curl --silent $img > $dest

kde=$(ps -A | grep 'kde'| wc -l)
gnome=$(ps -A | grep 'gnome' | wc -l)
if [ "$kde" != "0" ]
then
   dcop kdesktop KBackgroundIface 'setWallpaper(QString,int)' $dest 6 
echo "background image updated"
fi

if [ "$gnome" != "0" ]
then
gconftool-2 --type=string --set /desktop/gnome/background/picture_options stretched
gconftool-2 --type=string --set /desktop/gnome/background/picture_filename  $APOD_DIR/apod_$date.$ext
echo "background image updated"
fi
 echo -e '<HTML>\n <HEAD><TITLE>APOD</TITLE></HEAD>\n <BODY>' > $HOME/Desktop/apod_description.html
 curl --silent http://apod.nasa.gov/apod/ap$date.html | sed -n '/<b> Explanation:/,/<p> <center>/ p' | sed -n '2,$ p' | sed -e '$d' | sed -e '$d' >>  $HOME/Desktop/apod_description.html
 echo -e '</BODY> </HTML>' >>  $HOME/Desktop/apod_description.html
fi
fi
