#!/usr/bin/python

### add extra order, 3 or "all" to make a list with all polygons of both orders which would probably be most sensible anyhow.

from numpy import array
import numpy as N
import pyfits as pf
import sys

def tostr(ary): 
	print ary[0][0],
	for aa in ary[0][1:]:
		print ",",aa,
	for a in ary[1:]:
		for b in a:
			print ",",b, 

order=sys.argv[2]
try:
	order=int(order)
	all=False
except:
	if order=="all":
		all=True
	else:
		sys.stderr.write("Order is neither int nor 'all'.\n")
		sys.exit(2)
if order > 3:
	sys.stderr.write("Only order 1 and 2 permitted.\n")
	sys.exit(2)
if order==3:
	all=True


fil=pf.open(sys.argv[1])

listof=dict()
for tab in fil:
	if "SRC" in tab.name and "ORDER" in tab.name:
		listof[int(tab.name.split("_")[-1])]=fil.index_of(tab.name)

if not all:
	printdat=fil[listof[order]].data
else:
	printdat=array([])
	for tbid in listof.values():
		printdat=N.append(printdat,fil[tbid].data)
	
fil.close()	

Flag=False


if printdat[0][0][0]=="!":
	Flag=True

for dd in printdat:
	print dd[0].lower().replace("!","-"),"(",
	aa=array([dd[1],dd[2]])
	aa=aa.transpose()
	tostr(aa)
	print ")"
if Flag:
	sys.stderr.write("!!!!!! First element is a not, this could lead to trouble. Please move another line upwards \n")
	sys.stderr.write("!!!!!! We are living in folder"+os.getcwd()+" \n")




