This is a collection of various scripts I used during my PhD at SRON.

This work is licensed under the GPL v3.0. See the contents of the LICENSE file for the terms.
