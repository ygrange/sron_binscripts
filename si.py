#!/usr/bin/python
import math

def cgsflux(fin):
        fout = fin * 0.001
        return fout

def siflux(fin):
        fout = fin / 0.001
        return fout

def cgsnh(nhin):
        nhout = nhin * 10000
        return nhout

def sinh(nhin):
        nhout = nhin / 10000
        return nhout


print "What are you trying to convert to SI?"
print " (write negative value for opposite calculation)"
print "1. cgs flux"
print "2. cgs nH"
switch=raw_input("? ")

val=float(raw_input("input value "))
result={
'-1': lambda x : siflux(x),
'1': lambda x : cgsflux(x),
'-2': lambda x : sinh(x),
'2': lambda x : cgsnh(x) 
}[switch](val)

print result

