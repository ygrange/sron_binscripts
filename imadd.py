#!/usr/bin/python
"""

Adds up all images provided. The last command line argument is taken to be the target image.

"""
import pyfits as pf
import sys

def comp(a,b):
	for val in a:
		if val in b:
			return True
	return False


if comp(['h','help','-h','--help'],sys.argv[1:]) or (len(sys.argv) is 1) :
	print __doc__
	sys.exit(0)



outfile=sys.argv[-1]

adduppers=sys.argv[1:-1]
	
master=pf.open(adduppers[0])

for imf in adduppers[1:]:
	im=pf.open(imf)
	master[0].data=master[0].data+im[0].data
	im.close()
	

try:
	master.writeto(outfile)
	
except IOError, e:
	if "already exist" in str(e[0]):
		ans=raw_input("File "+str(outfile)+" already exists. Do you want me to try to overwrite?  ")
		if comp(ans.lower(),["y","yes","1"]):
			master.writeto(outfile,clobber=True)
	else:
		print "error occured while writing file. Try another filename/location"
